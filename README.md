#ConebeamRecon: GPU implemented joint cone-beam reconstruction and geometry estimation algorithms for Python

This module provides iterative reconstruction algorithms (OSEM with TV prior, SART, SIRT, OSSART/OSSIRT) with the option to include gradient-based methods to jointly estimate the true geometric parameters (Gradient Descent, Gradient Descent Line Search, Conjugate Gradient)


##Required Dependencies

- [PyCA](https://bitbucket.org/scicompanat/pyca) 0.01+
- [PyCUDA](http://wiki.tiker.net/PyCuda/Installation/Linux) 2013.1.1+
- [AppUtils](https://bitbucket.org/jhinkle/apputils) 0.01
- [ConebeamProjection](https://bitbucket.org/crottman/conebeamprojection)

##Optional Dependencies
- [PyCACalebExtras](https://bitbucket.org/crottman/pycacalebextras) (used for some plotting, examples, I/O, and some utility functions)

##Installation

Make sure PyCA, AppUtils, ConebeamProjection, and ConebeamRecon are all in your `$PYTHONPATH`.

##Usage

This package will both simulate datasets (if necessary) and run the reconstructions.
In order to see all the options, it may be helpful to look at `ReconConfig.py`.
A config object `cf` has three main sets of parameters: reconsturction algorithm parameters (`cf.Recon.*`, geometry estimation parameters (`cf.PoseEst.*`), and parameters for simulating a conebeam scan (`cf.ScanParam.*`).

Perhaps the best way to understand the usage of this module is to view the examples.
There are three fairly straightforward example programs that all work using a simple geometric phantom:

- `Example1.py`: Simple OS-SART reconstruction using mostly default parameters (no geometry estimation).
- `Example2.py`: OSEM+TV reconstruction with noisy projections (no geometry estimation).
- `Example3.py`: OSEM+TV reconstruction with and without geometry (translation)  estimation.

##Reporting

You can optionally create a report at the end of the run by making a report object defined in `ReconReport.py`.
This will create a HTML report with useful pictures, parameters, and graphs.
