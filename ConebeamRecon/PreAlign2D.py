import sys
import numpy as np
from PyCAApps.TranslationReg import TranslationReg, TemplateNCC
import PyCACalebExtras.Common as cc
import PyCACalebExtras.Display as cd
import PyCA.Core as ca


def PreAlign2D(projlist, cfglist, dataset):
    '''aligns the projections in 2D based on neighboring projections'''
    import matplotlib.pyplot as plt

    method = 'SSE'

    Tx = []
    Ty = []

    for proj in projlist:
        proj.toType(ca.MEM_DEVICE)

    if method == 'VE':
        projlistVE = [proj.copy() for proj in projlist]
        for proj, projVE in zip(projlist, projlistVE):
            cc.VarianceEqualize(projVE, proj, sigma=30, eps=.1)

        for i, (projS, projT) in enumerate(zip(projlistVE[:], projlistVE[1:])):
            print i, '/', len(projlistVE)-2
            plt.close('all')

            Aff = TranslationReg(projS, projT, plot=False)
            # print Aff
            Tx.append(-Aff[0, 2])
            Ty.append(-Aff[1, 2])
            # sys.exit()
            print Tx[-1], Ty[-1]

    elif method == 'NCC':
        for i, (projS, projT) in enumerate(zip(projlist[:], projlist[1:])):
            print i, '/', len(projlist)-2
            plt.close('all')

            xrng = [int(.3*projS.size().x), int(.7*projS.size().x)]
            yrng = [int(.3*projS.size().y), int(.7*projS.size().y)]

            x, y = TemplateNCC(projS, projT, [xrng, yrng], 30)
            print x, y
            Tx.append(x*projS.spacing().x)
            Ty.append(y*projS.spacing().y)

    elif method == 'SSE':
        for i, (projS, projT) in enumerate(zip(projlist[:], projlist[1:])):
            print i, '/', len(projlist)-2
            plt.close('all')

            Aff = TranslationReg(projS, projT, plot=False)
            # print Aff
            Tx.append(-Aff[0, 2])
            Ty.append(-Aff[1, 2])
            # sys.exit()
            print Tx[-1], Ty[-1]

    for proj in projlist:
        proj.toType(ca.MEM_HOST)

    # T0 = Tx
    # Nt = len(T0)
    # N = Nt+1
    # K = np.zeros((N, N))
    # K[0:Nt, 0:Nt] = np.identity(Nt)
    # K[N-1, 0:N-1] = np.ones(Nt)
    # K[0:N-1, N-1] = np.ones(Nt)
    # y = np.zeros(N)
    # y[0:Nt] = T0
    # x = np.linalg.solve(K, y)
    # Tx = x[0:Nt]

    # T0 = Ty
    # Nt = len(T0)
    # N = Nt+1
    # K = np.zeros((N, N))
    # K[0:Nt, 0:Nt] = np.identity(Nt)
    # K[N-1, 0:N-1] = np.ones(Nt)
    # K[0:N-1, N-1] = np.ones(Nt)
    # y = np.zeros(N)
    # y[0:Nt] = T0
    # x = np.linalg.solve(K, y)

    # Ty = x[0:Nt]

    return np.array([Tx, Ty])


def ApplyPreAlign2D(projlist, cfglist, T, dataset):
    Tx = T[0, :]
    Ty = T[1, :]

    print Tx[-1]

    # Txsum = -3.81               # true
    # Tysum = -2.67
    # Txsum = -3.5               # better?
    # Tysum = -2.0               # i think best for hand for VE

    if dataset.lower() == 'hand':
        fix_end = True
        xstart = -3.81
        ystart = -2.67
        xend = -3.31
        yend = -11.69
    elif dataset.lower() == 'skull':
        fix_end = True
        xstart = -15.0
        ystart = 2.0
        xend = -3.0
        yend = 2.0
        Tx *= projlist[0].spacing().x
        Ty *= projlist[0].spacing().y
        for cf in cfglist:
            cf.cam_trans[2] += 4.0
        print 'transforming skull dataset'
    else:
        fix_end = False
        xstart = 0.
        ystart = 0.
        # xend = 0
        # yend = -11.69


    for i, cf in enumerate(cfglist[1:]):
        scale = -cf.cam_trans[2]/cf.sid
        Tx[i] += scale*Tx[i]
        Ty[i] += scale*Ty[i]

    Txoff = 0.0
    Tyoff = 0.0
    if fix_end:
        Txoff = (xend - xstart - sum(Tx))/len(Tx)
        Tyoff = (yend - ystart - sum(Ty))/len(Ty)
        for i, t in enumerate(Tx):
            Tx[i] += Txoff
            Ty[i] += Tyoff

    Txsum = xstart
    Tysum = ystart
    cfglist[0].cam_trans[0] = Txsum
    cfglist[0].cam_trans[1] = Tysum
    for i, cf in enumerate(cfglist[1:]):
        Txsum += Tx[i]
        Tysum += Ty[i]
        cf.cam_trans[0] = Txsum
        cf.cam_trans[1] = Tysum

    if dataset.lower == 'hand':
        for cf in cfglist:
            Tw = np.dot(cf.cam_rot, cf.cam_trans)
            Tw += np.array([-2, -2, -.5])
            cf.cam_trans = np.dot(cf.cam_rot.T, Tw)

    print Tx[-1]
