''''AffineReg.py - Caleb Rottman

Uses Quasi-Newton algorithm from "Structural and radiometric asymmetry
in brain images" - Joshi, et. al. 2003'''

import PyCA.Core as ca

import matplotlib.pyplot as plt
plt.ion()   # tell it to use interactive mode -- see results immediately
import numpy as np
from numpy.linalg import inv
from scipy.stats.mstats import gmean

import PyCACalebExtras.Common as cc
import PyCACalebExtras.Display as cd


def AffineReg(Is, It, A=None, constraint=None,
              maxIter=50, plot=True, autostop=True):
    '''Affine registration: returns the 2D or 3D estimated Affine
    Matrix.

    The affine transformation is defined by by the world coordinates
    of the images (using the grid.origin and grid.spacing of the images)

    A (optional) is an initial estimate of the affine matrix (forward
    matrix, not the inverse)

    The options for constraint are 'rigid', 'isotropic', or
    'sheerfree'.  If left as None, the full affine matrix is
    estimated.
    '''

    # select constraint
    rigid = isotropic = sheerfree = False
    if constraint is not None:
        if constraint == 'rigid':
            rigid = True
        elif constraint == 'isotropic':
            isotropic = True
        elif constraint == 'sheerfree':
            sheerfree = True
        else:
            raise Exception("Unknown constraint")

    # Initialize Variables
    Idef = ca.Image3D(Is.grid(), Is.memType())
    gradIs = ca.Field3D(Is.grid(), Is.memType())
    gradIdef = ca.Field3D(Is.grid(), Is.memType())
    Idef_x = ca.Image3D(Is.grid(), Is.memType())
    Idef_y = ca.Image3D(Is.grid(), Is.memType())
    h = ca.Field3D(Is.grid(), Is.memType())
    diff = ca.Image3D(Is.grid(), Is.memType())
    scratchI = ca.Image3D(Is.grid(), Is.memType())
    x = ca.Image3D(Is.grid(), Is.memType())
    y = ca.Image3D(Is.grid(), Is.memType())
    energy = [[]]

    # possible range for Is-It (for plotting)
    diffrng = [ca.MinMax(It)[0] - ca.MinMax(Is)[1],
               ca.MinMax(It)[1] - ca.MinMax(Is)[0]]
    # shrink a little
    diffrng = [0.8*diffrng[0] + .2*diffrng[1],
               0.2*diffrng[0] + .8*diffrng[1]]

    is3D = cc.Is3D(Is)
    is2D = not(is3D)

    # convert x, y to world coordinates
    origin = Is.grid().origin().tolist()
    spacing = Is.grid().spacing().tolist()
    ca.SetToIdentity(h)
    ca.Copy(x, h, 0)
    ca.Copy(y, h, 1)
    x *= spacing[0]
    y *= spacing[1]
    x += origin[0]
    y += origin[1]
    if is3D:
        z = ca.Image3D(Is.grid(), Is.memType())
        Idef_z = ca.Image3D(Is.grid(), Is.memType())
        ca.Copy(z, h, 2)
        z *= spacing[2]
        z += origin[2]

    if is2D:
        Vlen = 6
        Adim = 3
    else:
        Vlen = 12
        Adim = 4

    ca.Gradient(gradIs, Is)
    gradIs *= -1

    V = [ca.Image3D(Is.grid(), Is.memType()) for _ in xrange(Vlen)]
    if A is None:
        A = np.identity(Adim)
    Ainv = inv(A)
    a_k = _matToVec(Ainv)
    a_k_old = a_k.copy()

    for _it in xrange(maxIter):
        A = inv(Ainv)       # need forward for ApplyAffineReal
        cc.ApplyAffineReal(Idef, Is, A)
        ca.Sub(diff, It, Idef)

        cc.ApplyAffineReal(gradIdef, gradIs, A)
        ca.Copy(Idef_x, gradIdef, 0)
        ca.Copy(Idef_y, gradIdef, 1)
        if is3D:
            ca.Copy(Idef_z, gradIdef, 2)

        # Compute V
        if is2D:
            ca.Mul(V[0], Idef_x, x)
            ca.Mul(V[1], Idef_x, y)
            ca.Mul(V[2], Idef_y, x)
            ca.Mul(V[3], Idef_y, y)
            ca.Copy(V[4], Idef_x)
            ca.Copy(V[5], Idef_y)
        else:
            ca.Mul(V[0], Idef_x, x)
            ca.Mul(V[1], Idef_x, y)
            ca.Mul(V[2], Idef_x, z)
            ca.Mul(V[3], Idef_y, x)
            ca.Mul(V[4], Idef_y, y)
            ca.Mul(V[5], Idef_y, z)
            ca.Mul(V[6], Idef_z, x)
            ca.Mul(V[7], Idef_z, y)
            ca.Mul(V[8], Idef_z, z)
            ca.Copy(V[9], Idef_x)
            ca.Copy(V[10], Idef_y)
            ca.Copy(V[11], Idef_z)

        # Compute \int V^T V
        VtV = np.zeros((Vlen, Vlen))
        for i in xrange(Vlen):
            for j in xrange(Vlen):
                if i >= j:  # save computing, VtV is symmetric
                    ca.Mul(scratchI, V[i], V[j])
                    VtV[i, j] = VtV[j, i] = ca.Sum(scratchI)
        # compute diffVt
        diffVt = np.zeros(Vlen)
        for i in xrange(Vlen):
            ca.Mul(scratchI, diff, V[i])
            diffVt[i] = ca.Sum(scratchI)
        # update a_k
        a_k -= np.dot(inv(VtV), diffVt)
        Ainv = _vecToMat(a_k)

        # For all of these, we perform the projection on the
        # forward affine matrix, so it doesn't change the translation
        if rigid:
            A = inv(Ainv)
            U, s, Vstar = np.linalg.svd(A[0:Adim-1, 0:Adim-1])
            A[0:Adim-1, 0:Adim-1] = np.dot(U, Vstar) # just removing s
            a_k = _matToVec(inv(A))          # a_k is now rigid
        elif isotropic:
            # No local minimum guarantee
            A = inv(Ainv)
            U, s, Vstar = np.linalg.svd(A[0:Adim-1, 0:Adim-1])
            # Note: this isn't necessarily the *best* projection, just *a* projection
            s = gmean(s) # s replaced by its geometric mean
            A[0:Adim-1, 0:Adim-1] = np.dot(U, Vstar)*s
            a_k = _matToVec(inv(A))          # a_k is now rigid
        elif sheerfree:
            # No local minimum guarantee
            A = inv(Ainv)
            U, s, Vstar = np.linalg.svd(A[0:Adim-1, 0:Adim-1])
            # Polar decomposition of A
            A[0:Adim-1, 0:Adim-1] = np.dot(np.dot(U, Vstar), np.diag(s))
            a_k = _matToVec(inv(A))          # a_k is now rigid

        if plot:
            Ainv = _vecToMat(a_k)
            A = inv(Ainv)
            cc.ApplyAffineReal(Idef, Is, A)
            ca.Sub(diff, It, Idef)

            cd.DispImage(diff, title='diff', newFig=False, rng=diffrng)

        energy[0].append(ca.Sum2(diff))

        if autostop:
            # print energy[0][it-10] - energy[0][it]
            # if energy[0][it-10] - energy[0][it]  < energy[0][0] / 1000000.0:
            #     break

            if max(abs(a_k_old - a_k)) < 1e-2:
                break
            a_k_old = a_k.copy()

    # Final Plot
    if plot:
        cd.DispImage(Idef)
        cd.EnergyPlot(energy)

    return inv(_vecToMat(a_k))


def _matToVec(Ainv):
    '''Takes a square affine matrix and returns the affine 'vector'
    '''
    if Ainv.shape[0] == 3:         # 2D affine
        a_v = np.zeros(6)
        a_v[0:2] = Ainv[0, 0:2]
        a_v[2:4] = Ainv[1, 0:2]
        a_v[4:6] = Ainv[0:2, 2]
    elif Ainv.shape[0] == 4:         # 3D affine
        a_v = np.zeros(12)
        a_v[0:3] = Ainv[0, 0:3]
        a_v[3:6] = Ainv[1, 0:3]
        a_v[6:9] = Ainv[2, 0:3]
        a_v[9:12] = Ainv[0:3, 3]
    else:
        raise Exception("_matToVec: Ainv needs to be 3x3 or 4x4!")
    return a_v


def _vecToMat(a_v):
    '''Takes an affine 'vector' and returns the square affine matrix
    '''
    if a_v.shape[0] == 6:        # 2D affine
        Ainv = np.identity(3)
        Ainv[0, 0:2] = a_v[0:2]
        Ainv[1, 0:2] = a_v[2:4]
        Ainv[0:2, 2] = a_v[4:6]
    elif a_v.shape[0] == 12:        # 3D affine
        Ainv = np.identity(4)
        Ainv[0, 0:3] = a_v[0:3]
        Ainv[1, 0:3] = a_v[3:6]
        Ainv[2, 0:3] = a_v[6:9]
        Ainv[0:3, 3] = a_v[9:12]
    else:
        raise Exception("_vecToMat: a_v needs to be length 6 or 12!")
    return Ainv


def _main():
    '''runs some test cases'''
    plt.close('all')

    use2D = True
    # use2D = False

    if use2D:
        Adim = 3
        # grid = ca.GridInfo(ca.Vec3Di(100, 99, 1), ca.Vec3Df(1.2, 0.8, 1), ca.Vec3Df(-50, -40, 0))
        grid = ca.GridInfo(ca.Vec3Di(100, 100, 1), ca.Vec3Df(1, 1, 1), ca.Vec3Df(-50, -50, 0))
        Is = ca.Image3D(grid, ca.MEM_DEVICE)
        It = ca.Image3D(grid, ca.MEM_DEVICE)

        A = np.array([[1.1, .1, 8],
                      [-.4, .9, -9],
                      [0, 0, 1]])

        cc.CreateRect(Is, [30, 30], [70, 70])
        cc.ApplyAffineReal(It, Is, A)
    else:
        Adim = 4
        # grid = ca.GridInfo(ca.Vec3Di(63, 64, 65), ca.Vec3Df(1.2, 1.1, .8), ca.Vec3Df(-31, -32, -34))
        grid = ca.GridInfo(ca.Vec3Di(64, 64, 64), ca.Vec3Df(1, 1, 1), ca.Vec3Df(-32, -32, -32))
        Is = ca.Image3D(grid, ca.MEM_DEVICE)
        It = ca.Image3D(grid, ca.MEM_DEVICE)
        A = np.array([[1, .1, .25, 3], # ground truth
                      [-.2, 1, -.1, -5],
                      [-.35, .15, 1, 4],
                      [0, 0, 0, 1]])
        cc.CreateCuboid(Is, [24, 24, 24], [40, 40, 40])
        cc.ApplyAffineReal(It, Is, A)

    cd.DispImage(Is)
    cd.DispImage(It)

    # Aest = AffineReg(Is, It, plot=True)
    # Aest = AffineReg(Is, It, plot=True, constraint='rigid', maxIter=100)
    # Aest = AffineReg(Is, It, plot=True, constraint='isotropic', maxIter=100)
    Aest = AffineReg(Is, It, plot=True, constraint='sheerfree', maxIter=100)

    print A
    print Aest
    print np.linalg.det(A[0:Adim-1, 0:Adim-1])
    print np.linalg.det(Aest[0:Adim-1, 0:Adim-1])


if __name__ == "__main__":
    _main()
