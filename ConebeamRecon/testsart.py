import PyCACalebExtras.SetBackend
plt = PyCACalebExtras.SetBackend.SetBackend()
import PyCACalebExtras.Common as cc
import PyCACalebExtras.Display as cd
import PyCA.Core as ca
import PyCA.Common as common
import numpy as np
# import PyCA.Common as common
import matplotlib.pyplot as plt
plt.ion()


def project(vol, dim):
    'projection op'
    return np.sum(vol, dim)


def backproject(im, dim):
    'backprojection op'
    if dim == 0:
        return np.array([im, im])
    else:
        return np.array([im, im]).T


trueI = np.array([[1.0, 2.0],
                  [3.0, 4.0]])

f0 = project(trueI, 0)
f1 = project(trueI, 1)
projs = [f0, f1]

# print trueI
# print '----'
# print f0
# print backproject(f0, 0)
# print '-----'
# print f1
# print backproject(f1, 1)

I = np.zeros((2, 2))

for i in xrange(10):
    update = np.zeros((2, 2))
    for k, proj in enumerate(projs):
        update += backproject( (proj - project(I, k))/project(np.ones((2, 2)), k), k)

    I += .5 * update
    print I

print '------'
print trueI
print '------'

# EM
I = .01*np.ones((2,2))
for i in xrange(100):
    update = np.zeros((2,2))
    updatediv = np.zeros((2,2))
    for k, proj in enumerate(projs):
        update += backproject( (proj/project(I, k)), k)
        updatediv += backproject( np.ones(2).T, k)
    I = I*update/updatediv
    print I
