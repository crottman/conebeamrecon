from AppUtils import Config
# import ReconPoseEst
from ReconPoseEst import vec_to_Rmat
import ReconConfig
import CreateProjData
import copy
import sys
import PyCACalebExtras.Display as cd
import PyCACalebExtras.Common as cc
import PyCA.Core as ca
import ConebeamProjection as cp

import numpy as np

import matplotlib.pyplot as plt
plt.ion() # tell it to use interactive mode -- see results immediately
plt.close('all')
import ReconReport
report = ReconReport.report()



# Make the scan Config object
cfscan = Config.SpecToConfig(ReconConfig.ReconConfigSpecs)
cfscan.ScanParam.numProj = 15
cfscan.ScanParam.scanRange = 360
cfscan.ScanParam.SID = 1000
cfscan.InputFile = '/home/sci/crottman/GE/CT/CTphantom.mha'
# cfscan.OutputFile = 'Results/NewCS/GivenVol_estSID_GDLSarmijosmart'
cfscan.OutputFile = 'Results/OldCS/test'

# cfscan.ScanParam.Noise = 'Correlated'
# cfscan.ScanParam.ZeroMeanNoise = True

cfscan.PoseEst.EstMethod = 'GDLS'
cfscan.PoseEst.LineSearchMethod = 'armijo'
# cfscan.PoseEst.EstEvery = 1

cfscan.OSEM.numIters = 20
cfscan.OSEM.EstVol = False
# cfscan.OSEM.TVweight = 0.01


# cfscan.OutputEvery = 100

# extrinsic parameters
# cfscan.ScanParam.Tsigma = [4, 4, 4]
cfscan.ScanParam.Tsigma = 1
cfscan.ScanParam.Rsigma = .1
# cfscan.ScanParam.Rsigma = [.6, .6, .6]

# intrinsic parameters
# cfscan.ScanParam.PPsigma = 3.0
cfscan.ScanParam.SIDsigma = 3.0


# get scan config list
cfglist_given, cfglist_true = CreateProjData.SetupScan(cfscan)
cfglist = copy.deepcopy(cfglist_given) # estimates are put in here

projlist, volest = CreateProjData.ProjectData(cfglist_true, cfscan)


# ReconPoseEst.RunRecon(volest, cfglist, projlist, cfscan, report=report, saveIters=False)

# Check Gradient Magnitudes
mult = np.zeros(len(projlist))

# for idx in xrange(9):
for idx in xrange(3,6):
    print "Testing Gradient " + str(idx)
    for k in xrange(len(projlist)):
        def Energy(v):
            testcfg = copy.deepcopy(cfglist[k])
            testcfg.cam_trans = cfglist[k].cam_trans + v[0:3]
            W = vec_to_Rmat(v[3:6])
            testcfg.cam_rot = np.dot(W, cfglist[k].cam_rot)
            testcfg.piercing_point = cfglist[k].piercing_point + v[6:8]
            testcfg.sid = cfglist[k].sid + v[8]
            en = cp.ConebeamSumSquaredErrorFast(projlist[k], volest, testcfg)
            return en
        step = .001
        # decrease step for Rotation
        if 2 < idx < 6:
            step /= 10.0
        if idx == 8:
            step *= 10
        test1 = np.zeros(9)
        test2 = np.zeros(9)
        test1[idx] = step
        test2[idx] = -step
        stepgrad = (Energy(test1) - Energy(test2))/step/2.0 # delx grad
        grads = cp.ConebeamPoseGradient(projlist[k], volest, cfglist[k]) # calc grad


        print k
        print 'delta grad:', stepgrad
        print 'calc. grad:', grads[idx]
        mult[k] = stepgrad/grads[idx]
        print 'mult. fact:', mult[k]
        # print 'rev. mult.:', grads[idx]/stepgrad
        print 'Energy:', grads[9]


    plt.figure('Gradient Direction ' + str(idx))
    plt.plot(mult, 'r')
    plt.plot(np.zeros(len(projlist)) + np.mean(mult), 'g')
    plt.ylim(0, 2)
    plt.show()
