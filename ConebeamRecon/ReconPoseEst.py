'''Performs an Ordered Subset Reconstruction of conebeam data while
performing a pose estimation of the data

This module requires ConebeamProjection'''
import sys
import time
import numpy as np
from numpy.linalg import norm
import copy
from scipy.optimize import brent

import PyCA.Core as ca
import ConebeamProjection as cp
import PyCACalebExtras.Common as cc
import PyCACalebExtras.SetBackend
import PyCACalebExtras.Display as cd
from ConebeamRecon.LineSearch import line_search_armijo

eps = 1.e-5                 # for safe division vol update


def _safelen(item):
    '''returns the length, or if the item isn't iterable, returns 1'''
    try:
        return len(item)
    except TypeError:
        return 1


def _safermlist(item):
    '''if the item is iterable and has length 1, return the only element'''
    if _safelen(item) == 1:
        try:
            return item[0]
        except TypeError:
            pass
    return item


def RunRecon(projcfglist, projlist, cf, masklist=None, volest=None,
             Disp=False, report=None):
    '''Runs conebeam reconstruction with (optional) parameter estimation

    Required parameters:
    - projcfglist: N length list of projection config parameters
        (the config is defined in the ConebeamProjection module)
        These internal values will be updated if you are estimating parameters
    - projlist: N legnth list of projection PyCA Image3Ds
    - cf: reconstruction config object (defined in ConebeamRecon module)

    Optional parameters:
    - masklist: N length list of binary mask images relating to the projections
        data won't be used where masklist is 0. This only works ok...
    - volest: the estimated volume (Image3D). This can be passed in, otherwise
        it will be created, and it's apart of the report
    - Disp: show a figure of the reconstruction every iteration
    - report: object to hang on to reporting values (that will write a report later)
    '''

    # if single scale, remove all (possible) extraneous lists
    # if multiscale, make sure everything is in an n length list

    # if multiscale, parameters that must already be lists:
    # Recon.numIters,
    # PoseEst.{Tmaxpert, RmaxPert, PPmaxPert, SIDMaxPert}
    # should be changed to lists:
    # PoseEst.{Tstep, Rstep, PPstep, SIDStep}

    if masklist is None:
        masklist = [None]*len(projlist)
    useMask = masklist[0] is not None

    # Set up volest
    if volest is None:
        if cf.Recon.VolSpacing is None:
            cf.Recon.VolSpacing = [1, 1, 1]
        if cf.Recon.VolOrigin is None:
            cf.Recon.VolOrigin = [-(1.0*sz-1)/2.0*sp for sz, sp in
                                  zip(cf.Recon.VolSize, cf.Recon.VolSpacing)]
        volgrid = ca.GridInfo(ca.Vec3Di(*cf.Recon.VolSize),
                              ca.Vec3Df(*cf.Recon.VolSpacing),
                              ca.Vec3Df(*cf.Recon.VolOrigin))
        volest = ca.Image3D(volgrid, ca.MEM_DEVICE)
    else:
        if cf.Recon.VolSpacing is None:
            cf.Recon.VolSpacing = volest.spacing().tolist()
        if cf.Recon.VolOrigin is None:
            cf.Recon.VolOrigin = volest.origin().tolist()
        if cf.Recon.VolSize is None:
            cf.Recon.VolSize = volest.size().tolist()

    # allow non-unique scales, e.g. [3, 3, 2, 2, 1]
    numScales = _safelen(cf.Recon.numIters)  # e.g. = 5
    numIters = _safermlist(cf.Recon.numIters)  # e.g. = [10, 20, 20]
    if numScales == 1:
        totalIters = numIters
        # make sure we don't have a list of a single item
        cf.PoseEst.Tstep = _safermlist(cf.PoseEst.Tstep)
        cf.PoseEst.Rstep = _safermlist(cf.PoseEst.Rstep)
        cf.PoseEst.PPstep = _safermlist(cf.PoseEst.PPstep)
        cf.PoseEst.SIDstep = _safermlist(cf.PoseEst.SIDstep)
        cf.PoseEst.TmaxPert = _safermlist(cf.PoseEst.TmaxPert)
        cf.PoseEst.RmaxPert = _safermlist(cf.PoseEst.RmaxPert)
        cf.PoseEst.PPmaxPert = _safermlist(cf.PoseEst.PPmaxPert)
        cf.PoseEst.SIDmaxPert = _safermlist(cf.PoseEst.SIDmaxPert)
        cf.Recon.TVweight = _safermlist(cf.Recon.TVweight)

        projlist_current = projlist
        masklist_current = masklist
        if cf.Recon.EstVol:
            ca.SetMem(volest, 0.001)

        scale = _safermlist(cf.Recon.scales)
        useManager = scale is not None and scale != 1
        scaleIds = [0]

    else:
        numScales = len(numIters)

        # Change default value to list
        # if these parameters are given, they must already be iterable
        if cf.PoseEst.Tstep is None:
            cf.PoseEst.Tstep = [None]*numScales
        if cf.PoseEst.Rstep is None:
            cf.PoseEst.Rstep = [None]*numScales
        if cf.PoseEst.PPstep is None:
            cf.PoseEst.PPstep = [None]*numScales
        if cf.PoseEst.SIDstep is None:
            cf.PoseEst.SIDstep = [None]*numScales
        if cf.PoseEst.TmaxPert <= 0.0:
            cf.PoseEst.TmaxPert = [0.0]*numScales
        if cf.PoseEst.RmaxPert <= 0.0:
            cf.PoseEst.RmaxPert = [0.0]*numScales
        if cf.PoseEst.PPmaxPert <= 0.0:
            cf.PoseEst.PPmaxPert = [0.0]*numScales
        if cf.PoseEst.SIDmaxPert <= 0.0:
            cf.PoseEst.SIDmaxPert = [0.0]*numScales
        if _safelen(cf.Recon.TVweight) == 1:
            cf.Recon.TVweight = [cf.Recon.TVweight]*numScales

        if cf.Recon.scales is None:
            cf.Recon.scales = [2**i for i in reversed(xrange(len(numIters)))]

        useManager = not max(cf.Recon.scales) == min(cf.Recon.scales) == 1

        scales = cf.Recon.scales
        totalIters = sum(numIters)
        uniqueScales = sorted(set(cf.Recon.scales), reverse=True)  # e.g. [3, 2, 1]
        scaleIds = [uniqueScales.index(s) for s in scales]  # e.g. [0,0,1,2,2]

        origProjGrid = projlist[0].grid()
        origVolGrid = volest.grid()
        projManager = ca.MultiscaleManager(origProjGrid)
        volManager = ca.MultiscaleManager(origVolGrid)
        if useManager:
            for s in uniqueScales:
                projManager.addScaleLevel(s)
                volManager.addScaleLevel(s)
            projResampler = ca.MultiscaleResamplerGaussGPU(origProjGrid)
            volResampler = ca.MultiscaleResamplerGaussGPU(origVolGrid)

            # projlist_current = [proj.copy() for proj in projlist]
            projlist_current = []
            for proj in projlist:
                projlist_current.append(proj.copy())
                proj.toType(ca.MEM_HOST)
            if useMask:
                # masklist_current = [mask.copy() for mask in masklist]
                masklist_current = []
                for mask in masklist:
                    masklist_current.append(mask.copy())
                    mask.toType(ca.MEM_HOST)
            else:
                masklist_current = masklist

            def setScale(scale):
                '''downsamples/upsamples as necessary'''

                projManager.set(scale)
                volManager.set(scale)
                projResampler.setScaleLevel(projManager)
                volResampler.setScaleLevel(volManager)

                # upsample volest
                if volManager.isFirstScale():
                    volest.setGrid(volManager.getCurGrid())
                    if cf.Recon.EstVol:
                        ca.SetMem(volest, 0.001)
                else:
                    # volResampler.upsampleImage(volest)
                    volest.setGrid(volManager.getCurGrid())
                    if cf.Recon.EstVol:
                        ca.SetMem(volest, 0.001)

                # downsample projection
                for projCurr, projOrig in zip(projlist_current, projlist):
                    projOrig.toType(ca.MEM_DEVICE)
                    projCurr.toType(ca.MEM_DEVICE)
                    projResampler.downsampleImage(projCurr, projOrig)
                    projOrig.toType(ca.MEM_HOST)

                # downsample mask
                if useMask:
                    for mask, maskOrig in zip(masklist_current, masklist):
                        maskOrig.toType(ca.MEM_DEVICE)
                        mask.toType(ca.MEM_DEVICE)
                        projResampler.downsampleImage(mask, maskOrig)
                        maskOrig.toType(ca.MEM_HOST)

                    # re-figure out mask
                    for mask, proj in zip(masklist_current, projlist_current):
                        maskweight = mask.copy()
                        cc.SetRegionLT(maskweight, maskweight, 1e-5, 1e-5)
                        proj /= maskweight
                        cc.SetRegionLTE(mask, mask, 0.2, 0)
                        cc.SetRegionGTE(mask, mask, 0.2, 1)
                        proj *= mask

        else:
            projlist_current = projlist
            if cf.Recon.EstVol:
                ca.SetMem(volest, 0.001)
            masklist_current = [None]*len(projlist)

    # assert that scales are fine
    if numScales > 1:
        if _safelen(cf.PoseEst.Tstep) != numScales:
            raise RuntimeError("Tstep is of length {0}, should be of length {1}"
                               .format(_safelen(cf.PoseEst.Tstep), numScales))
        if _safelen(cf.PoseEst.Rstep) != numScales:
            raise RuntimeError("Rstep is of length {0}, should be of length {1}"
                               .format(_safelen(cf.PoseEst.Rstep), numScales))
        if _safelen(cf.PoseEst.PPstep) != numScales:
            raise RuntimeError("PPstep is of length {0}, should be of length {1}"
                               .format(_safelen(cf.PoseEst.PPstep), numScales))
        if _safelen(cf.PoseEst.SIDstep) != numScales:
            raise RuntimeError("SIDstep is of length {0}, should be of length {1}"
                               .format(_safelen(cf.PoseEst.SIDstep), numScales))
        if _safelen(cf.PoseEst.TmaxPert) != numScales:
            raise RuntimeError("TmaxPert is of length {0}, should be of length {1}"
                               .format(_safelen(cf.PoseEst.TmaxPert), numScales))
        if _safelen(cf.PoseEst.RmaxPert) != numScales:
            raise RuntimeError("RmaxPert is of length {0}, should be of length {1}"
                               .format(_safelen(cf.PoseEst.RmaxPert), numScales))
        if _safelen(cf.PoseEst.PPmaxPert) != numScales:
            raise RuntimeError("PPmaxPert is of length {0}, should be of length {1}"
                               .format(_safelen(cf.PoseEst.PPmaxPert), numScales))
        if _safelen(cf.PoseEst.SIDmaxPert) != numScales:
            raise RuntimeError("SIDmaxPert is of length {0}, should be of length {1}"
                               .format(_safelen(cf.PoseEst.SIDmaxPert), numScales))

    if report is not None:
        if report.x_trans is None:
            report.init_arrays()
        report.start_time = time.time()

    if not cf.Recon.EstVol:
        Disp = False
        cf.SaveIters = False         # just in case

    # make sure SART/SIRT/OSSART/OSSIRT matches with NumOS
    if cf.Recon.method == 'SART':
        cf.Recon.numOS = len(projlist)
    elif cf.Recon.method == 'SIRT':
        cf.Recon.numOS = 1

    for i, scale in enumerate(scaleIds):
        if useManager:
            if i == 0 or scale != scaleIds[i-1]:  # scale is unique
                setScale(scale)
            else:
                print "level is {}. Not re/up-sampling anything".format(scale)
            # parameters for current scale
        if numScales > 1:
            dScale = dict(prevIters=sum(numIters[0:i]),
                          currIters=numIters[i],
                          totalIters=totalIters,
                          TVweight=cf.Recon.TVweight[i],
                          Tstep=cf.PoseEst.Tstep[i],
                          Rstep=cf.PoseEst.Rstep[i],
                          PPstep=cf.PoseEst.PPstep[i],
                          SIDstep=cf.PoseEst.SIDstep[i],
                          TmaxPert=cf.PoseEst.TmaxPert[i],
                          RmaxPert=cf.PoseEst.RmaxPert[i],
                          PPmaxPert=cf.PoseEst.PPmaxPert[i],
                          SIDmaxPert=cf.PoseEst.SIDmaxPert[i])
        else:
            dScale = dict(prevIters=0,
                          currIters=numIters,
                          totalIters=totalIters,
                          TVweight=cf.Recon.TVweight,
                          Tstep=cf.PoseEst.Tstep,
                          Rstep=cf.PoseEst.Rstep,
                          PPstep=cf.PoseEst.PPstep,
                          SIDstep=cf.PoseEst.SIDstep,
                          TmaxPert=cf.PoseEst.TmaxPert,
                          RmaxPert=cf.PoseEst.RmaxPert,
                          PPmaxPert=cf.PoseEst.PPmaxPert,
                          SIDmaxPert=cf.PoseEst.SIDmaxPert)

        RunReconScale(volest, projcfglist, projlist_current, cf, masklist_current, dScale,
                      Disp, report)


def RunReconScale(volest, projcfglist, projlist, cf, masklist, dScale, Disp=True,
                  report=None):
    '''Main function: runs a reconstruction for a specific scale
    '''
    # set up grad mask
    if cf.PoseEst.UseGradMask:
        gradmask = CalcGradMask(projlist, volest, projcfglist, masklist)
    else:
        gradmask = None

    N = len(projlist)               # number of projections
    # max value of all projections: use this to cap volest
    # (this is a very hacky)
    maxP = max(ca.Max(p) for p in projlist)

    # get scale specific parameters from dict
    prevIters = dScale['prevIters']
    currIters = dScale['currIters']
    totalIters = dScale['totalIters']
    Tstep = dScale['Tstep']
    Rstep = dScale['Rstep']
    PPstep = dScale['PPstep']
    SIDstep = dScale['SIDstep']
    lmda = dScale['TVweight']
    TmaxPert = np.array(dScale['TmaxPert'], float)  # list compare doesn't work
    RmaxPert = np.array(dScale['RmaxPert'], float)
    PPmaxPert = np.array(dScale['PPmaxPert'], float)
    SIDmaxPert = float(dScale['SIDmaxPert'])

    # get constant parameters from config
    numOS = cf.Recon.numOS
    EstVol = cf.Recon.EstVol
    EstEvery = cf.PoseEst.EstEvery
    EstMethod = cf.PoseEst.EstMethod
    LineSearchMethod = cf.PoseEst.LineSearchMethod
    # EstIters = cf.PoseEst.EstIters # might not use
    OutputEvery = cf.OutputEvery
    if OutputEvery is None:
        OutputEvery = totalIters  # don't output before end
    # EM = cf.Recon.method == 'OSEM'

    # give warning if Tstep and Tmaxpert are both set
    if Tstep is not None and norm(TmaxPert) > 0:
        print "Warning! TmaxPert ignored!"
    if Rstep is not None and norm(RmaxPert) > 0:
        print "Warning! RmaxPert ignored!"
    if PPstep is not None and norm(PPmaxPert) > 0:
        print "Warning! PPmaxPert ignored!"
    if SIDstep is not None and SIDmaxPert > 0:
        print "Warning! SIDmaxPert ignored!"

    # subsets is a list of subset lists
    subsets = [range(iOS, N, numOS) for iOS in range(numOS)]
    # reorder using bit-reversal
    subsets = [subsets[i] for i in bit_reversal(len(subsets))]

    # don't bother getting the pose gradient if MaxPerts are all 0
    if Tstep in (None, 0.0):
        Tstep = np.array([0.0, 0.0, 0.0])
    if Rstep in (None, 0.0):
        Rstep = np.array([0.0, 0.0, 0.0])
    if PPstep in (None, 0.0):
        PPstep = np.array([0.0, 0.0])
    if SIDstep in (None, 0.0):
        SIDstep = 0.0

    GetTestGradient = any((norm(TmaxPert) > 0, norm(RmaxPert) > 0,
                           norm(PPmaxPert) > 0, SIDmaxPert > 0))
    GetPoseGradient = any((norm(Tstep) > 0, norm(Rstep) > 0,
                           norm(PPstep) > 0, SIDstep > 0, GetTestGradient))

    # Get Grad magnitudes right before you start estimating (to determine initial step size)
    # However, don't estimate on iteation 0, where the volume is constant
    testGradIter = max(EstEvery - 1, 1)

    # data structures for CG
    if EstMethod == 'CG':
        delxprev = np.zeros((9, N))  # \Delta x_{n-1}, for CG
        sprev = np.zeros((9, N))  # s_{n-1}, for CG

    # Option to stop estimating current pose (can only be false if estvol=False)
    EstProjPose = [True] * N      # start with estimating every projection parameters

    for it_small in xrange(currIters):  # it_small is the subset iteration
        it_big = it_small + prevIters   # it_big is the overall iteration

        print 'iter', it_big
        # iterate through all subsets

        if it_small == testGradIter:
            if GetTestGradient:
                print 'testing gradients'
                # only test certain gradients:

                # Find maximum gradients
                maxGradT = maxGradR = maxGradPP = maxGradSID = 0.0
                for k in xrange(N):
                    # Get Max gradient
                    grads = Gradient(cf, projlist[k], volest, projcfglist[k],
                                     gradmask, masklist[k])
                    if cc.IsNumber(TmaxPert):  # maxGradT is a scalar
                        maxGradT = np.maximum(maxGradT, norm(grads[0:3]))
                    else:      # maxGradT is an vector, smart gradient
                        maxGradT = np.maximum(maxGradT, abs(grads[0:3]))
                    if cc.IsNumber(RmaxPert):  # maxGradR is a scalar
                        maxGradR = max(maxGradR, norm(grads[3:6]))
                    else:      # maxGradR is an vector, smart gradient
                        maxGradR = np.maximum(maxGradR, abs(grads[3:6]))
                    maxGradPP = np.maximum(maxGradPP, abs(grads[6:8]))
                    # maxGradPP = max(maxGradPP, norm(grads[6:8]))  # maxGradPP is a scalar
                    maxGradSID = max(maxGradSID, abs(grads[8]))
                Tstep = TmaxPert/maxGradT
                Rstep = RmaxPert/maxGradR
                PPstep = PPmaxPert/maxGradPP
                SIDstep = SIDmaxPert/maxGradSID
            else:
                if GetPoseGradient:
                    print 'not testing gradients - step already set'
                else:
                    print 'not testing gradients'
                if Tstep is not None:
                    Tstep = np.array(Tstep, np.float64)
                if Rstep is not None:
                    Rstep = np.array(Rstep, np.float64)
                if PPstep is not None:
                    PPstep = np.array(PPstep, np.float64)
                if SIDstep is not None:
                    SIDstep = np.array(SIDstep, np.float64)

            if norm(Tstep) > 0:
                print 'Tstep:', Tstep
            if norm(Rstep) > 0:
                print 'Rstep:', Rstep
            if SIDstep > 0:
                print 'SIDstep:', SIDstep
            if norm(PPstep) > 0:
                print 'PPstep:', PPstep

            # only test certain gradients (in the future)
            # this only applies for param est + NCC
            calcParamIndex = [True]*10
            calcParamIndex[0:3] = Tstep
            calcParamIndex[3:6] = [Rstep, Rstep, Rstep]
            calcParamIndex[6:8] = PPstep
            calcParamIndex[8] = SIDstep

        # estimate pose for all projections (after max gradients are calculated)
        EstPoseNow = it_small > testGradIter and GetPoseGradient and it_small % EstEvery == 0

        if EstMethod == 'GD':   # Gradient Descent
            # go through each subset
            for subset in subsets:
                for k in subset:
                    # if EstPoseNow and subset[0]%2 == 0:
                    if EstPoseNow:
                        # Get the gradient if you need to estimate any of the pose params
                        grads = Gradient(cf, projlist[k], volest, projcfglist[k],
                                         gradmask, masklist[k], calcParamIndex)
                        # Apply step size (possibly vector)
                        g = grads[0:3]
                        g *= Tstep                            # Tstep is a vector
                        grads[0:3] = g
                        grads[3:6] *= Rstep
                        grads[6:8] *= PPstep
                        grads[8] *= SIDstep

                        # apply update (step is already applied)
                        projcfglist[k].cam_trans -= grads[0:3]
                        if norm(RmaxPert) > 0:
                            W = vec_to_Rmat(-grads[3:6])
                            projcfglist[k].cam_rot = np.dot(W, projcfglist[k].cam_rot)
                        projcfglist[k].piercing_point -= grads[6:8]
                        projcfglist[k].sid -= grads[8]

                        if report is not None:
                            report.energy_full[k, it_big] = grads[9]
                            report.R_gradmag_full[k, it_big] = norm(grads[3:6])*180/np.pi

                # # For GD, Run OSEM Update after a subset's parameters have been estimated
                # if EstVol:
                #     SubsetUpdate(subset, volest, projlist, projcfglist, cf, lmda, N, maxP)
            if EstPoseNow:
                CalcObjective(report, it_big+0.5, projlist, volest,
                              projcfglist, cf, masklist, lmda)

            for subset in subsets:
                if EstVol:
                    SubsetUpdate(subset, volest, projlist, projcfglist, cf,
                                 masklist, lmda, N, maxP)

            CalcObjective(report, it_big+1, projlist, volest,
                          projcfglist, cf, masklist, lmda)

            CalcL2Distance(report, volest)  # for convergence properties

        elif EstMethod == 'GDLS' or EstMethod == 'CG':
            if EstPoseNow:
                # Estimate all poses at once (ordered subsets makes too big of a change)
                total_ests = sum(EstProjPose)  # how many projs we are about to estimate
                total_calls = 0               # we'll use this to print info later

                # zprimesum = 0.0
                for k in xrange(N):
                    if not EstProjPose[k]:
                        continue

                    # Energy considers the current point to be 0
                    def myEnergy(v):
                        '''energy function w.r.t. parameters'''
                        testcfg = copy.deepcopy(projcfglist[k])
                        testcfg.cam_trans = projcfglist[k].cam_trans + v[0:3]
                        if norm(Rstep) > 0:  # save some computations by checking first
                            W = vec_to_Rmat(v[3:6])
                            testcfg.cam_rot = np.dot(W, projcfglist[k].cam_rot)
                        testcfg.piercing_point = projcfglist[k].piercing_point + v[6:8]
                        testcfg.sid = projcfglist[k].sid + v[8]

                        return Energy(cf, projlist[k], volest, testcfg, gradmask, masklist[k])
                    grads = Gradient(cf, projlist[k], volest, projcfglist[k],
                                     gradmask, masklist[k], calcParamIndex)
                    Curr_energy = grads[9]
                    # print grads
                    # Curr_energy = Energy(cf, projlist[k], volest, projcfglist[k])
                    # print Curr_energy

                    # Apply Steps now so that grad vector is pointing in the right dir
                    g = grads[0:3]
                    # adjust g gradient for "step size"
                    g *= Tstep                            # Tstep is a vector
                    delx = np.zeros(9)  # not sure if necessary
                    delx[0:3] = -g
                    delx[3:6] = -Rstep*grads[3:6]
                    delx[6:8] = -PPstep*grads[6:8]
                    delx[8] = -SIDstep*grads[8]
                    if (Tstep == 0).all():
                        grads[0:3] *= 0
                    if (Rstep == 0).all():
                        grads[3:6] *= 0
                    if (PPstep == 0).all():
                        grads[6:8] *= 0
                    if SIDstep == 0:
                        grads[8] *= 0

                    # alpha0 = 20  # first line search size
                    alpha0 = 5  # first line search size


                    if EstMethod == 'CG':
                        # # B^FR - really bad
                        # denom = np.dot(delxprev[:,k], delxprev[:,k])
                        # if abs(denom) > 1e-6:
                        #     Beta = np.dot(delx, delx)/ denom
                        # else:
                        #     Beta = 0
                        # # B^PR -
                        # denom = np.dot(delxprev[:,k], delxprev[:,k])
                        # if abs(denom) > 1e-6:
                        #     Beta = np.dot(delx, delx - delxprev[:,k]) / denom
                        # else:
                        #     Beta = 0
                        # B^HS - best
                        # denom = np.dot(sprev[:,k], delx - delxprev[:,k])
                        # if abs(denom) > 1e-8:
                        #     Beta = -np.dot(delx, delx - delxprev[:,k]) / denom
                        # else:
                        #     Beta = 0
                        # # B^DY - worst
                        denom = np.dot(sprev[:, k], delx - delxprev[:, k])
                        if abs(denom) > 1e-6:
                            Beta = np.dot(delx, delx) / denom
                        else:
                            Beta = 0

                        Beta = max(Beta, 0)  # reset if need be

                        if Beta == 0 and k == 0:
                            print 'Resetting Beta'
                        sn = delx + Beta*sprev[:, k]

                        delxprev[:, k] = delx
                        sprev[:, k] = sn
                    elif EstMethod == 'GDLS':
                        sn = delx
                        Beta = 0  # just used for stopping criteria

                    if LineSearchMethod == 'brent':
                        def myEnergy1(alpha):
                            '''energy with just a single argument'''
                            return myEnergy(sn*alpha)
                        out = brent(myEnergy1, (), brack=[0, alpha0],
                                    full_output=True, tol=.01, maxiter=8)
                        alpha, f_val_at_alpha, iters, f_count = out
                        total_calls += f_count
                    elif LineSearchMethod == 'armijo':
                        out = line_search_armijo(myEnergy, np.zeros(9), sn,
                                                 grads[0:9], Curr_energy, alpha0=alpha0)
                        alpha, f_count, f_val_at_alpha = out
                        if alpha is None:
                            print "Can't find a suitable step length (proj {})".format(k)
                            alpha = 0.0
                        total_calls += f_count
                    else:
                        raise Exception("Unknown LineSearchMethod")

                    # if it_big in [3, 70]:  # armijo quadratic search debugging
                    #     import matplotlib.pyplot as plt
                    #     plt.ion()
                    #     print 'step', sn*alpha
                    #     # real energy values - blue
                    #     plt.figure(11)
                    #     arng = np.linspace(0, alpha0, 25)
                    #     fvals = [myEnergy(sn*al) for al in arng]
                    #     plt.plot(arng, fvals, 'b', label='True Energy')
                    #     # solve for parabola
                    #     fprime = np.dot(grads[0:9], sn)
                    #     a = (fvals[-1] - fprime*alpha0 - fvals[0])/alpha0**2
                    #     b = fprime
                    #     c = fvals[0]
                    #     quadmin = -fprime*alpha0**2/2.0/(fvals[-1]-fprime*alpha0-fvals[0])
                    #     print 'min at alph=', quadmin
                    #     plt.plot(arng, [a*x**2 + b*x + c for x in arng], 'r',
                    #              label='Estimated Energy')
                    #     # wolfe condition line
                    #     c1 = 1e-1
                    #     plt.plot(arng, [fvals[0] + c1*quadmin*fprime]*len(arng), 'g',
                    #              label='Wolff')
                    #     plt.legend(fontsize=10)
                    #     print alpha, f_count, f_val_at_alpha
                    #     plt.show()
                    #     plt.draw()
                    #     plt.pause(0.0000001)
                    #     raw_input()
                    #     plt.clf()

                    # stop if steps are too small (be stricter for rotation steps)
                    if all([max(abs(sn[3:6]*alpha)) < 1e-5, max(abs(sn*alpha)) < .01,
                            Beta == 0, not EstVol]):
                        EstProjPose[k] = False  # stop estimating
                        print("no longer updating projection ", k,
                              "still updating ", str(sum(EstProjPose)) + '/' + str(N))
                    # # don't allow it to take a yuge step
                    # if np.linalg.norm(sn[0:3]*alpha) > 4:
                    #     alpha = 0.0
                    projcfglist[k].cam_trans += sn[0:3]*alpha          # apply update
                    if norm(Rstep) > 0:
                        W = vec_to_Rmat(sn[3:6]*alpha)
                        projcfglist[k].cam_rot = np.dot(W, projcfglist[k].cam_rot)
                    projcfglist[k].piercing_point += sn[6:8]*alpha
                    projcfglist[k].sid += sn[8]*alpha

                    # zprimesum += sn[3]*alpha

                    if report is not None:
                        report.R_gradmag_full[k, it_big] = norm(sn[3:6]*alpha)*180/np.pi
                        report.energy_full[k, it_big] = Curr_energy
                if total_ests > 0:
                    print 'Average Line search energy calls:', float(total_calls)/total_ests

                # print zprimesum

            # even if we don't est pose, do OSEM
            if EstVol and not EstMethod == 'GD':  #
                if EstPoseNow:
                    CalcObjective(report, it_big+0.5, projlist, volest,
                                  projcfglist, cf, masklist, lmda)

                for subset in subsets:
                    SubsetUpdate(subset, volest, projlist, projcfglist, cf,
                                 masklist, lmda, N, maxP)

                CalcObjective(report, it_big+1, projlist, volest,
                              projcfglist, cf, masklist, lmda)

                CalcL2Distance(report, volest)  # for convergence properties

        else:
            raise Exception("Unknown EstMethod")

        if report is not None:
            for k in xrange(N):
                # fill in following estimates (that remain the same)
                for l in xrange(it_big, min(it_big+EstEvery, totalIters)):
                    report.x_trans_full[k, l] = projcfglist[k].cam_trans[0]
                    report.y_trans_full[k, l] = projcfglist[k].cam_trans[1]
                    report.z_trans_full[k, l] = projcfglist[k].cam_trans[2]
                    report.u_time_full[k, l] = projcfglist[k].piercing_point[0]
                    report.v_time_full[k, l] = projcfglist[k].piercing_point[1]
                    report.sid_time_full[k, l] = projcfglist[k].sid
                    report.R_time_full[k, l, :, :] = projcfglist[k].cam_rot
            # Write Report
            if ((it_small+1) % OutputEvery == 0 and it_small > 0) or it_big == totalIters-1:
                report.volest = volest
                report.run_time = time.time()-report.start_time
                report.cfglist = projcfglist
                report.x_trans = report.x_trans_full[:, :it_big+1]  # only fill in estimated
                report.y_trans = report.y_trans_full[:, :it_big+1]
                report.z_trans = report.z_trans_full[:, :it_big+1]
                report.R_gradmag = report.R_gradmag_full[:, :it_big+1]
                report.energy = report.energy_full[:, :it_big+1]
                report.u_time = report.u_time_full[:, :it_big+1]
                report.v_time = report.v_time_full[:, :it_big+1]
                report.sid_time = report.sid_time_full[:, :it_big+1]
                report.R_time = report.R_time_full[:, :it_big+1, :, :]

                if it_big == totalIters-1:  # save GPU memory
                    for proj in projlist:
                        proj.toType(ca.MEM_HOST)

                PyCACalebExtras.SetBackend.SetBackend('cairo')
                report.saveReport()  # save the objects
                report.makeReport()  # create the HTML report

                if it_big == totalIters-1:  # save GPU memory
                    for proj in projlist:
                        proj.toType(ca.MEM_DEVICE)

        if Disp:                # displays the estimate each iteration
            print 'Range of estimated volume: ', ca.MinMax(volest)
            if it_big == 0:
                cd.DispImage(volest, newFig=True)
            cd.DispImage(volest, newFig=False)
        if cf.SaveIters:
            cc.mkdir(cf.OutputFile + '/iter_images/')
            itername = cf.OutputFile + '/iter_images/iter{:03}.png'.format(it_big)
            cc.WritePNG(volest, itername, rng=[0, 1])


def vec_to_Rmat(v):
    '''takes a rotation gradient v (that maps via the star operator to a
    skew symmetric matrix) and a step (for how long along the geodesic
    to travel) and returns the equivalent rotation matrix that should be
    multiplied by the parameter rotation matrix

    '''
    v = v.astype(np.float64)    # change to double
    vmag = norm(v)
    if vmag < 1e-10:            # too small
        return np.identity(3)
    v /= vmag
    vvt = np.outer(v, v)
    starv = np.array([[0, -v[2], v[1]],
                      [v[2], 0, -v[0]],
                      [-v[1], v[0], 0]])
    W = np.identity(3) + np.sin(vmag)*starv + \
        (1-np.cos(vmag))*(vvt - np.identity(3))
    return W


def SubsetUpdate(subset, volest, projlist, projcfglist, cf,
                 masklist, lmda, N, maxP):
    '''generic subset update, will perform using EM or SART'''
    if cf.Recon.method == 'OSEM':
        EMSubsetUpdate(subset, volest, projlist, projcfglist, cf, masklist, lmda, N)
        cc.SetRegionGT(volest, volest, maxP, maxP)
    else:
        SARTSubsetUpdate(subset, volest, projlist, projcfglist, cf, masklist, lmda, N)


def EMSubsetUpdate(subset, volest, projlist, projcfglist, cf, masklist, lmda, N):
    '''perform an OSEM update for a particular subset'''

    # # Since our background condition for BP is ones, P1sum is just a scalar
    # P1sum = float(len(subset))
    P1sum = ca.Image3D(volest.grid(), volest.memType())
    ca.SetMem(P1sum, 0.0)
    voltmp = ca.Image3D(volest.grid(), volest.memType())

    # # try to get rid of some edge artifacts: doesn't do much,
    # # probably better to just clamp
    # FP1 = ca.Image3D(volest.grid(), volest.memType())  # test
    # ca.SetMem(FP1, 1.0)                                # test

    # OSEM update for images in current subset
    ca.SetMem(voltmp, 0.0)
    for k in subset:
        imgrid = projlist[k].grid()
        imtmp = ca.Image3D(imgrid, volest.memType())
        imtmp2 = ca.Image3D(imgrid, volest.memType())
        imtmp3 = ca.Image3D(imgrid, volest.memType())
        # FP1proj = ca.Image3D(imgrid, volest.memType())  # test

        ds = min(volest.spacing().tolist())

        # sum of backprojections
        if cf.Recon.POperator.lower() == 'lineint':
            cp.ConebeamProject(imtmp, volest, projcfglist[k], ds)
            # cp.ConebeamProject(FP1proj, FP1, projcfglist[k], ds)  # test
        elif cf.Recon.POperator.lower() == 'facelineint':
            cp.ConebeamProjectFast(imtmp, volest, projcfglist[k])
            # cp.ConebeamProjectFast(FP1proj, FP1, projcfglist[k])  # test
        else:
            raise NotImplementedError("Unknown projection operator")

        ca.Copy(imtmp3, projlist[k])
        cc.SetRegionLT(imtmp3, projlist[k], eps, eps, scratchI=imtmp2)
        cc.SetRegionLT(imtmp, imtmp, eps, eps, scratchI=imtmp2)  # avoid NAN

        # cc.SetRegionGTE(FP1proj, FP1proj, 1.0, 1.0)  # test

        ca.Div(imtmp2, imtmp3, imtmp)  # proj / P(I)
        # imtmp2 *= FP1proj       # test

        if cf.Recon.BPOperator.lower() == 'voxel':
            cp.ConebeamBackproject(voltmp, imtmp2, projcfglist[k],
                                   masklist[k], addResult=True)
            cp.ConebeamBackproject(P1sum, projlist[k], projcfglist[k], masklist[k],
                                   addResult=True, onesOnly=True)
        elif cf.Recon.BPOperator.lower() == 'splat':
            cp.ConebeamBackprojectSplat(voltmp, imtmp2, projcfglist[k], ds,
                                        masklist[k], addResult=True)
            cp.ConebeamBackprojectSplat(P1sum, imtmp2, projcfglist[k], ds,
                                        masklist[k], addResult=True, onesOnly=True)
        else:
            raise NotImplementedError("Unknown backprojection operator")

    # Re-estimate the volume
    if lmda > 0.0:
        dU = ca.Image3D(volest.grid(), volest.memType())
        cp.ComputeTVDerivative(dU, volest, eps)  # figure out checkerboard?
        # scale/subset based lambda
        dU *= 1.0 * lmda * len(subset)**2/N

        # clear tv derivative for non-visited voxels
        cc.SetRegionLTE(dU, P1sum, eps, 0.0)
        P1sum += dU

    # cc.SetRegionLTE(voltmp, voltmp, eps, eps)  # floor voltmp for non-zero
    # cc.SetRegionLTE(P1sum, P1sum, eps, eps)  # floor P1sum for save divide
    cc.SetRegionLTE(voltmp, voltmp, 1.0, 1.0)  # floor voltmp for non-zero
    cc.SetRegionLTE(P1sum, P1sum, 1.0, 1.0)  # floor P1sum for save divide
    voltmp /= P1sum                          # this is good, and non-zero
    volest *= voltmp

    cc.SetRegionLT(volest, volest, eps, eps)


def SARTSubsetUpdate(subset, volest, projlist, projcfglist, cf, masklist, lmda, N):
    '''perform an SART update for a particular subset'''

    BP1sum = ca.Image3D(volest.grid(), volest.memType())
    # ones3D = ca.Image3D(volest.grid(), volest.memType())
    # ca.SetMem(ones3D, 1.0)
    ca.SetMem(BP1sum, 0.0)
    voltmp = ca.Image3D(volest.grid(), volest.memType())

    # SART update for images in current subset
    ca.SetMem(voltmp, 0.0)
    for k in subset:
        imgrid = projlist[k].grid()
        imtmp = ca.Image3D(imgrid, volest.memType())
        imtmp2 = ca.Image3D(imgrid, volest.memType())
        imtmp3 = ca.Image3D(imgrid, volest.memType())

        # calculate difference and projectin of ones
        ds = min(volest.spacing().tolist())
        if cf.Recon.POperator.lower() == 'lineint':
            cp.ConebeamProject(imtmp, volest, projcfglist[k], ds)
            # cp.ConebeamProject(imtmp3, ones3D, projcfglist[k], ds)
            cp.ConebeamProject(imtmp3, volest, projcfglist[k], ds, onesOnly=True)
        elif cf.Recon.POperator.lower() == 'facelineint':
            cp.ConebeamProjectFast(imtmp, volest, projcfglist[k])
            # cp.ConebeamProjectFast(imtmp3, ones3D, projcfglist[k])
            cp.ConebeamProjectFast(imtmp3, volest, projcfglist[k], onesOnly=True)
        else:
            raise NotImplementedError("Unknown projection operator")

        ca.Sub(imtmp2, projlist[k], imtmp)  # difference
        cc.SetRegionLT(imtmp3, imtmp3, 0.1, 0.1)
        imtmp2 /= imtmp3

        if cf.Recon.BPOperator.lower() == 'voxel':
            cp.ConebeamBackproject(voltmp, imtmp2, projcfglist[k],
                                   masklist[k], addResult=True)
            cp.ConebeamBackproject(BP1sum, projlist[k], projcfglist[k], masklist[k],
                                   addResult=True, onesOnly=True)
        elif cf.Recon.BPOperator.lower() == 'splat':
            cp.ConebeamBackprojectSplat(voltmp, imtmp2, projcfglist[k], ds,
                                        masklist[k], addResult=True)
            cp.ConebeamBackprojectSplat(BP1sum, imtmp2, projcfglist[k], ds,
                                        masklist[k], addResult=True, onesOnly=True)
        else:
            raise NotImplementedError("Unknown backprojection operator")

    # # Re-estimate the volume
    # if lmda > 0.0:
    #     dU = ca.Image3D(volest.grid(), volest.memType())
    #     cp.ComputeTVDerivative(dU, volest, eps)  # figure out checkerboard?
    #     # scale/subset based lambda
    #     dU *= 1.0 * lmda * len(subset)**2/N

    #     # clear tv derivative for non-visited voxels
    #     cc.SetRegionLTE(dU, P1sum, eps, 0.0)
    #     P1sum += dU

    cc.SetRegionLTE(BP1sum, BP1sum, 1.0, 1.0)  # floor P1sum for save divide
    voltmp /= BP1sum                          # this is good, and non-zero
    voltmp *= cf.Recon.RelaxWeight
    volest += voltmp


def CalcGradMask(projlist, volest, cfglist, masklist):
    gradmask = ca.Image3D(volest.grid(), volest.memType())
    ca.SetMem(gradmask, 0.0)

    for proj, cf, mask in zip(projlist, cfglist, masklist):
        cp.ConebeamBackproject(gradmask, proj, cf, mask,
                               addResult=True, onesOnly=True)

    gradmask /= len(cfglist)
    cc.SetRegionGTE(gradmask, gradmask, 0.9, 1.0)
    cc.SetRegionLTE(gradmask, gradmask, 0.9, 0.0)

    cube = gradmask.copy()
    cc.CreateCuboid(cube, [3, 3, 3],
                    [gradmask.size().x-3, gradmask.size().y-3, gradmask.size().z-3])
    gradmask *= cube
    # gradmask *= 0.0

    return gradmask


def CalcObjective(report, it, projlist, volest, projcfglist, cf, masklist, lmda):
    '''Calculates the objective generically. Adds either log-posterior
    to report (OSEM) or weighted penalty (S*RT)'''
    if report is None:
        return

    if cf.Recon.method == 'OSEM':
        report.LL[0].append(it)
        report.LL[1].append(CalcLogPosterior(projlist, volest, projcfglist,
                                             cf, masklist, lmda))
    else:
        report.Norm[0].append(it)
        report.Norm[1].append(CalcWeightedNorm(projlist, volest, projcfglist,
                                               cf, masklist))


def CalcLogPosterior(projlist, volest, projcfglist, cf, masklist, lmda):
    '''calculates the log posterior for TV-EM'''
    ll = 0.0
    imtmp = projlist[0].copy()
    ds = min(volest.spacing().tolist())

    for proj, cfg, mask in (zip(projlist, projcfglist, masklist)):
        if cf.Recon.POperator.lower() == 'lineint':
            cp.ConebeamProject(imtmp, volest, cfg, ds)
        elif cf.Recon.POperator.lower() == 'facelineint':
            cp.ConebeamProjectFast(imtmp, volest, cfg)

        if mask is not None:
            imtmp *= mask

        ll -= ca.Sum(imtmp)

        cc.SetRegionLT(imtmp, imtmp, eps, eps)
        ca.Log_I(imtmp)
        imtmp *= proj
        if mask is not None:
            imtmp *= mask
        ll += ca.Sum(imtmp)

    if lmda > 0:
        mag = ca.Image3D(volest.grid(), volest.memType())
        grad = ca.Field3D(volest.grid(), volest.memType())
        ca.Gradient(grad, volest, ca.DIFF_FORWARD)
        ca.Magnitude(mag, grad)
        tvpen = lmda*ca.Sum(mag)
        ll -= tvpen

    return ll/projlist[0].nVox()


def CalcWeightedNorm(projlist, volest, projcfglist, cf, masklist):
    '''calculates the W-norm for S*RT'''
    norm = 0.0
    imtmp = projlist[0].copy()
    imtmp2 = projlist[0].copy()
    ds = min(volest.spacing().tolist())

    for proj, cfg, mask in (zip(projlist, projcfglist, masklist)):
        if cf.Recon.POperator.lower() == 'lineint':
            cp.ConebeamProject(imtmp, volest, cfg, ds)
            cp.ConebeamProject(imtmp2, volest, cfg, ds, onesOnly=True)
        elif cf.Recon.POperator.lower() == 'facelineint':
            cp.ConebeamProjectFast(imtmp, volest, cfg)
            cp.ConebeamProject(imtmp2, volest, cfg, ds, onesOnly=True)
        cc.SetRegionLT(imtmp2, imtmp2, 0.1, 0.1)

        imtmp -= proj
        if mask is not None:
            imtmp *= mask
        imtmp /= imtmp2
        norm += ca.Sum2(imtmp)

    return norm/projlist[0].nVox()


def CalcL2Distance(report, volest):
    '''calculates the L2 distance between the estimate and
    the true image'''
    if report.voltrue is None:
        return
    if report.voltrue.grid() != volest.grid():
        return
    tmp = ca.Image3D(volest.grid(), volest.memType())
    ca.Sub(tmp, volest, report.voltrue)
    report.L2.append(np.sqrt(ca.Sum2(tmp)))

    # print 'orig error', np.sqrt(ca.Sum2(report.voltrue))


def Gradient(cf, proj, volest, projcfg, gradmask, mask, calcParamIndex=None):
    if cf.PoseEst.Metric.upper() == 'SSE':
        return cp.ConebeamPoseGradient(proj, volest,
                                       projcfg, gradmask, mask)

    elif cf.PoseEst.Metric.upper() == 'NCC':
        return cp.ConebeamPoseGradientNCC(proj, volest, projcfg,
                                          gradmask, mask, calcParamIndex)
    else:
        raise NotImplementedError("Unknown metric '{}'".format(cf.PoseEst.Metric))


def Energy(cf, proj, volest, cfg, gradmask=None, mask=None):
    if cf.PoseEst.Metric.upper() == 'SSE':
        if cf.Recon.POperator.lower() == 'facelineint':
            en = cp.ConebeamSumSquaredErrorFast(proj, volest, cfg)
        elif cf.Recon.POperator.lower() == 'lineint':
            en = cp.ConebeamSumSquaredError(proj, volest, cfg)
        else:
            raise NotImplementedError("Unknown Projection Operator {}"
                                      .format(cf.Recon.POperator))
    elif cf.PoseEst.Metric.upper() == 'NCC':
        en = cp.ConebeamNCC(proj, volest, cfg, gradmask, mask)
    else:
        raise NotImplementedError("Unknown metric '{}'".format(cf.PoseEst.Metric))
    return en


def bit_reversal(N):
    '''given a total number, returns a range of 0 to N-1
    reorganized via bit-reversal'''
    L = [range(N)]
    maxLen = max(len(l) for l in L)
    while maxLen > 1:
        for l in L:
            if len(l) == maxLen:
                L.append(l[len(l)/2:])
                del l[len(l)/2:]
        maxLen = max(len(l) for l in L)
    return [l[0] for l in L]
