import numpy as np
import shutil
import errno
import os
from subprocess import call


def ViewScan2D(cflist, scanconfig=None, cflist_comp=None,
               plotDetector=False, plotvol=False, every=1):
    '''view 2D scan, optionally takes a scan config object. plots the following:

    1) source to piercing point for each projection
       if scanconfig is given, this line is source to origin of detector
    2) blue lines connecting all the sources
    3) (optional) bounding box of the volume
    4) (optional) Detector bounds
    '''
    import matplotlib.pyplot as plt

    plt.figure()
    for i, cf in enumerate(cflist):
        if i % every != 0:
            continue

        # display path of source-to-detector-origin
        sourceC = np.array([0.0, 0.0, 0.0])

        # this is the origin of the detector CS in camera coords
        dCentC = np.array([-cf.piercing_point[0], -cf.piercing_point[1], cf.sid])
        R = np.array(cf.cam_rot)
        T = np.array(cf.cam_trans)

        # convert to world
        source = np.dot(R, sourceC + T)
        dCentW = np.dot(R, dCentC + T)

        plt.plot([source[0], dCentW[0]],
                 [source[1], dCentW[1]], '-r')
        if i > 0:
            plt.plot([source[0], prevsource[0]],
                     [source[1], prevsource[1]], '-b')
        prevsource = source

        # # plot point closest to 0,0 for each projection
        # px0, py0 = source[0:2]
        # px1, py1 = pp[0:2]
        # a = py0-py1
        # b = px1-px0
        # c = -(px1-px0)*py0+(py1-py0)*px0
        # x1 = -a*c/(a*a+b*b)
        # y1 = -b*c/(a*a+b*b)
        # if i > 0:
        #     plt.plot([x1, prevx1],
        #              [y1, prevy1], '-b')
        # prevx1, prevy1 = x1, y1
        # plt.plot([x1, 0],
        #          [y1, 0], 'g')
        # Plot intersection points
        # if i>0:
        #     x1, y1 = source[0:2]
        #     x2, y2 = pp[0:2]
        #     x3, y3 = prevsource[0:2]
        #     x4, y4 = prevpp[0:2]
        #     intersection = [((x1*y2-y1*x2)*(x3-x4)-(x1-x2)*(x3*y4-y3*x4))/
        #                     ((x1-x2)*(y3-y4)-(y1-y2)*(x3-x4)),
        #                     ((x1*y2-y1*x2)*(y3-y4)-(y1-y2)*(x3*y4-y3*x4))/
        #                     ((x1-x2)*(y3-y4)-(y1-y2)*(x3-x4))]
        #     if i>every:
        #         plt.plot([intersection[0], previntersection[0]],
        #                  [intersection[1], previntersection[1]], '-g')
        #     previntersection = intersection

        # prevsource = source
        # prevpp = pp

    if cflist_comp is not None:  # same as before, except green
        for i, cf in enumerate(cflist_comp):
            if i % every != 0:
                continue

            sourceC = np.array([0.0, 0.0, 0.0])

            # this is the origin of the detector CS in camera coords
            dCentC = np.array([-cf.piercing_point[0], -cf.piercing_point[1], cf.sid])
            R = np.array(cf.cam_rot)
            T = np.array(cf.cam_trans)
            # convert to world
            source = np.dot(R, sourceC + T)
            dCentW = np.dot(R, dCentC + T)
            plt.plot([source[0], dCentW[0]],
                     [source[1], dCentW[1]], '-g')
            if i > 0:
                plt.plot([source[0], prevsource[0]],
                         [source[1], prevsource[1]], '-g')
            prevsource = source

    # plot volume box
    if scanconfig is not None and plotvol:
        origin = scanconfig.Recon.VolOrigin
        spacing = scanconfig.Recon.VolSpacing
        size = scanconfig.Recon.VolSize
        ll = [origin[0], origin[1]]
        lr = [origin[0]+size[0]*spacing[0], origin[1]]
        ur = [origin[0]+size[0]*spacing[0], origin[1]+size[1]*spacing[1]]
        ul = [origin[0], origin[1]+size[1]*spacing[1]]
        plt.plot([ll[0], lr[0]],
                 [ll[1], lr[1]], '-b')
        plt.plot([ll[0], ul[0]],
                 [ll[1], ul[1]], '-b')
        plt.plot([ul[0], ur[0]],
                 [ul[1], ur[1]], '-b')
        plt.plot([ur[0], lr[0]],
                 [ur[1], lr[1]], '-b')

    if scanconfig is not None and plotDetector:
        # plot first detector, assuming it is in-line w/ world coords

        cf = cflist[0]
        dSp = np.array(scanconfig.ScanParam.ProjSpacing)
        dSz = np.array(scanconfig.ScanParam.ProjSize)
        dOr = np.array(scanconfig.ScanParam.ProjOrigin)
        # origin of detector in camera coords
        minD = dOr
        maxD = dOr+dSp*(dSz-1)
        dCentC = np.array([-cf.piercing_point[0], -cf.piercing_point[1], cf.sid])
        dLeftC = dCentC - np.array([minD[0], minD[1], 0])
        dRightC = dCentC - np.array([maxD[0], maxD[1], 0])

        R = np.array(cf.cam_rot)
        T = np.array(cf.cam_trans)
        dLeftW = np.dot(R, dLeftC + T)
        dRightW = np.dot(R, dRightC + T)

        plt.plot([dLeftW[0], dRightW[0]],
                 [dLeftW[1], dRightW[1]], '-g')

    plt.xticks([])
    plt.yticks([])
    plt.axis('equal')
    plt.axis('off')


def ViewScan3D(cflist, scanconfig=None, plotDetector=False, plotvol=False, every=1):
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D, proj3d
    import mpl_toolkits.mplot3d as mp3d
    from itertools import product, combinations
    # from matplotlib.patches import FancyArrowPatch

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.set_aspect("equal")
    # ax.view_init(elev=21, azim=30)

    for i, cf in enumerate(cflist):
        if i % every != 0:
            continue

        # display path of source
        sourceC = np.array([0.0, 0.0, 0.0])
        # pp = np.array([0, 0, .7*cf.sid])
        dCentC = np.array([-cf.piercing_point[0], -cf.piercing_point[1], cf.sid])
        # dCentC = np.array([0, 0, cf.sid])
        R = np.array(cf.cam_rot)
        T = np.array(cf.cam_trans)

        sourceW = np.dot(R, sourceC + T)
        dCentW = np.dot(R, dCentC + T)

        if i == 0:
            xcam = np.dot(R, np.array([100, 0, 0]) + T)
            ycam = np.dot(R, np.array([0, 100, 0]) + T)
            xproj = np.dot(R, dCentC + np.array([100, 0, 0]) + T)
            yproj = np.dot(R, dCentC + np.array([0, 100, 0]) + T)
            # z direction
            ax.plot([sourceW[0], dCentW[0]],
                    [sourceW[1], dCentW[1]],
                    [sourceW[2], dCentW[2]], '-b')
            # coordinates 1
            ax.plot([sourceW[0], xcam[0]],
                    [sourceW[1], xcam[1]],
                    [sourceW[2], xcam[2]], '-r')
            ax.plot([sourceW[0], ycam[0]],
                    [sourceW[1], ycam[1]],
                    [sourceW[2], ycam[2]], '-g')
            # from center of detector to +x/+y
            ax.plot([dCentW[0], xproj[0]],
                    [dCentW[1], xproj[1]],
                    [dCentW[2], xproj[2]], '-r')
            ax.plot([dCentW[0], yproj[0]],
                    [dCentW[1], yproj[1]],
                    [dCentW[2], yproj[2]], '-g')

        else:
            ax.plot([sourceW[0], dCentW[0]],
                    [sourceW[1], dCentW[1]],
                    [sourceW[2], dCentW[2]], '-r')

        if i > 0:
            ax.plot([sourceW[0], prevsourceW[0]],
                    [sourceW[1], prevsourceW[1]],
                    [sourceW[2], prevsourceW[2]], '-b')
        prevsourceW = sourceW

    cflist_comp = None
    if cflist_comp is not None:
        for i, cf in enumerate(cflist_comp):
            if i % every != 0:
                continue

            # display path of source
            sourceW = np.array(cf.cam_trans)

            if i > 0:
                ax.plot([sourceW[0], prevsourceW[0]],
                        [sourceW[1], prevsourceW[1]],
                        [sourceW[2], prevsourceW[2]], '-g')
            prevsourceW = sourceW

    if scanconfig is not None and plotvol:
        origin = scanconfig.Recon.VolOrigin
        spacing = scanconfig.Recon.VolSpacing
        size = scanconfig.Recon.VolSize
        xmin, xmax = origin[0], origin[0]+(size[0]-1)*spacing[0]
        ymin, ymax = origin[1], origin[1]+(size[1]-1)*spacing[1]
        zmin, zmax = origin[2], origin[2]+(size[2]-1)*spacing[2]

        # +x, +y, +z coordinate axes, x, y, z
        ax.plot([xmin, xmax], [ymin, ymin], [zmin, zmin], '-r')
        ax.plot([xmin, xmin], [ymin, ymax], [zmin, zmin], '-g')
        ax.plot([xmin, xmin], [ymin, ymin], [zmin, zmax], '-b')
        # 9 other edges
        # bottom
        ax.plot([xmax, xmax], [ymin, ymax], [zmin, zmin], '-k')
        ax.plot([xmin, xmax], [ymax, ymax], [zmin, zmin], '-k')
        # top
        ax.plot([xmin, xmax], [ymin, ymin], [zmax, zmax], '-k')
        ax.plot([xmin, xmax], [ymax, ymax], [zmax, zmax], '-k')
        ax.plot([xmin, xmin], [ymin, ymax], [zmax, zmax], '-k')
        ax.plot([xmax, xmax], [ymin, ymax], [zmax, zmax], '-k')
        # up and down
        ax.plot([xmin, xmin], [ymax, ymax], [zmin, zmax], '-k')
        ax.plot([xmax, xmax], [ymax, ymax], [zmin, zmax], '-k')
        ax.plot([xmax, xmax], [ymin, ymin], [zmin, zmax], '-k')

    if scanconfig is not None and plotDetector:
        # plot first detector, assuming it is in-line w/ world coords

        cf = cflist[0]
        dSp = np.array(scanconfig.ScanParam.ProjSpacing)
        dSz = np.array(scanconfig.ScanParam.ProjSize)
        dOr = np.array(scanconfig.ScanParam.ProjOrigin)
        # origin of detector in camera coords
        minD = dOr
        maxD = dOr+dSp*(dSz-1)
        dCentC = np.array([-cf.piercing_point[0], -cf.piercing_point[1], cf.sid])
        dLLC = dCentC - np.array([minD[0], minD[1], 0])
        dLRC = dCentC - np.array([minD[0], maxD[1], 0])
        dULC = dCentC - np.array([maxD[0], minD[1], 0])
        dURC = dCentC - np.array([maxD[0], maxD[1], 0])
        # dLeftC = dCentC - np.array([minD[0], minD[1], 0])
        # dRightC = dCentC - np.array([maxD[0], maxD[1], 0])

        R = np.array(cf.cam_rot)
        T = np.array(cf.cam_trans)
        # dLeftW = np.dot(R, dLeftC + T)
        # dRightW = np.dot(R, dRightC + T)
        dLLW = np.dot(R, dLLC + T)
        dLRW = np.dot(R, dLRC + T)
        dULW = np.dot(R, dULC + T)
        dURW = np.dot(R, dURC + T)

        ax.plot([dLLW[0], dLRW[0]], [dLLW[1], dLRW[1]], [dLLW[2], dLRW[2]], '-k')
        ax.plot([dLLW[0], dULW[0]], [dLLW[1], dULW[1]], [dLLW[2], dULW[2]], '-k')
        ax.plot([dURW[0], dLRW[0]], [dURW[1], dLRW[1]], [dURW[2], dLRW[2]], '-k')
        ax.plot([dURW[0], dULW[0]], [dURW[1], dULW[1]], [dURW[2], dULW[2]], '-k')
        # plt.plot([dLeftW[0], dRightW[0]],
        #          [dLeftW[1], dRightW[1]], '-g')

    # AXIS HACK!
    xmin, xmax = ax.get_xlim3d()
    ymin, ymax = ax.get_ylim3d()
    zmin, zmax = ax.get_zlim3d()
    xc = np.average((xmin, xmax))
    yc = np.average((ymin, ymax))
    zc = np.average((zmin, zmax))
    maxsq = max(xmax - xmin, ymax-ymin, zmax-zmin)
    ax.set_xlim3d(xc - maxsq/2, xc + maxsq/2)
    ax.set_ylim3d(yc - maxsq/2, yc + maxsq/2)
    ax.set_zlim3d(zc - maxsq/2, zc + maxsq/2)

    ax.set_xticklabels([])
    ax.set_yticklabels([])
    ax.set_zticklabels([])

    ax.set_aspect("equal")


def ViewProjection(cf, proj, vol, handle=None):
    from mayavi import mlab     # could fail otherwise

    # mlab.clf()

    # Everything is dones in real coords

    sid = cf.sid
    cam_rot = np.array(cf.cam_rot)
    T = cf.cam_trans

    # Source to PP Points
    source = np.array([0, 0, 0])
    pp = np.array([0, 0, sid])
    # apply transformation
    source = np.dot(cam_rot, source + T)
    pp = np.dot(cam_rot, pp + T)

    # Detector Points
    imszx = proj.spacing().x*proj.size().x
    imszy = proj.spacing().y*proj.size().y

    det_ll = np.array([proj.origin().x, proj.origin().y, sid])
    det_lr = np.array([proj.origin().x + imszx, proj.origin().y, sid])
    det_ur = np.array([proj.origin().x + imszx, proj.origin().y+imszy, sid])
    det_ul = np.array([proj.origin().x, proj.origin().y+imszy, sid])
    # apply transformation
    det_ll = np.dot(cam_rot, det_ll + T)
    det_lr = np.dot(cam_rot, det_lr + T)
    det_ur = np.dot(cam_rot, det_ur + T)
    det_ul = np.dot(cam_rot, det_ul + T)

    # Volume points
    szx = vol.spacing().x*vol.size().x
    szy = vol.spacing().y*vol.size().y
    szz = vol.spacing().z*vol.size().z
    vox = vol.origin().x
    voy = vol.origin().y
    voz = vol.origin().z
    v0 = np.array([vox, voy, voz])
    v1 = np.array([vox+szx, voy, voz])
    v2 = np.array([vox, voy+szy, voz])
    v3 = np.array([vox+szx, voy+szy, voz])
    v4 = np.array([vox, voy, voz+szz])
    v5 = np.array([vox+szx, voy, voz+szz])
    v6 = np.array([vox, voy+szy, voz+szz])
    v7 = np.array([vox+szx, voy+szy, voz+szz])

    # Plotting

    # plot volume
    pts = []
    for i in xrange(3):
        pts.append([v0[i], v1[i], v2[i], v3[i], v4[i], v5[i], v6[i], v7[i]])
    triangles = [(0, 1, 5), (0, 4, 5),
                 (0, 2, 6), (0, 4, 6),
                 (0, 1, 3), (0, 2, 3),
                 (7, 5, 1), (7, 3, 1),
                 (7, 5, 4), (7, 6, 4),
                 (7, 3, 2), (7, 6, 2)]
    mlab.triangular_mesh(pts[0], pts[1], pts[2], triangles, color=(.5, .5, .8))

    # plot projection area
    pts = []
    for i in xrange(3):
        pts.append([source[i], det_ll[i], det_lr[i], det_ur[i], det_ul[i]])
    triangles = [(0, 1, 2), (0, 2, 3), (0, 3, 4), (0, 4, 1)]
    mlab.triangular_mesh(pts[0], pts[1], pts[2], triangles, color=(1, .5, 0), opacity=.3)

    # plot axes
    axlen = 2*max(szz, szy, szx)
    pts = [[0, axlen], [0, 0], [0, 0]]
    mlab.plot3d(pts[0], pts[1], pts[2], tube_radius=10.0, color=(1, .5, 0))
    pts = [[0, 0], [0, axlen], [0, 0]]
    mlab.plot3d(pts[0], pts[1], pts[2], tube_radius=10.0, color=(1, .5, 0))
    pts = [[0, 0], [0, 0], [0, axlen]]
    mlab.plot3d(pts[0], pts[1], pts[2], tube_radius=10.0, color=(1, .5, 0))

    # plot source to piercing point
    pts = [[], [], []]
    for i in xrange(3):
        pts[i] = [source[i], pp[i]]
    mlab.plot3d(pts[0], pts[1], pts[2], tube_radius=8.0)

    # plot image grid
    pts = [[], [], []]
    for i in xrange(3):
        pts[i] = [det_ll[i], det_lr[i], det_ur[i], det_ul[i], det_ll[i]]
    mlab.plot3d(pts[0], pts[1], pts[2], tube_radius=8.0)

    # plot the volume grid

    # mlab.axes(xlabel='x', ylabel='y', zlabel='z', ranges=[-100, 100, -100, 100, -100, 100])
    return mlab.gcf()


def ViewProjection3D(cf, proj, vol):
    from mpl_toolkits.mplot3d import Axes3D, proj3d
    from itertools import product, combinations
    from matplotlib.patches import FancyArrowPatch
    import matplotlib.pyplot as plt

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.set_aspect("equal")
    ax.view_init(elev=21, azim=30)

    class Arrow3D(FancyArrowPatch):
        def __init__(self, xs, ys, zs, *args, **kwargs):
            FancyArrowPatch.__init__(self, (0, 0), (0, 0), *args, **kwargs)
            self._verts3d = xs, ys, zs

        def draw(self, renderer):
            xs3d, ys3d, zs3d = self._verts3d
            xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, renderer.M)
            self.set_positions((xs[0], ys[0]), (xs[1], ys[1]))
            FancyArrowPatch.draw(self, renderer)

    # World Coords
    a = Arrow3D([0, 0], [0, 0], [0, 150], mutation_scale=20, lw=1,
                arrowstyle="-|>", color="k")
    ax.add_artist(a)
    ax.text(0, 0, 150, 'z', None)
    a = Arrow3D([0, 0], [0, 150], [0, 0], mutation_scale=20, lw=1,
                arrowstyle="-|>", color="k")
    ax.add_artist(a)
    ax.text(0, 150, 0, 'y', None)
    a = Arrow3D([0, 150], [0, 0], [0, 0], mutation_scale=20, lw=1,
                arrowstyle="-|>", color="k")
    ax.add_artist(a)
    ax.text(150, 0, 0, 'x', None)
    # ax.invert_zaxis()
    # ax.invert_xaxis()

    ax.set_xlim(-600, 600)
    ax.set_ylim(-600, 600)
    ax.set_zlim(-600, 600)

    # Camera Coords
    co = np.dot(cf.cam_rot, cf.cam_trans)
    xp = np.dot(cf.cam_rot, np.array([150, 0, 0]) + cf.cam_trans)
    yp = np.dot(cf.cam_rot, np.array([0, 150, 0]) + cf.cam_trans)
    zp = np.dot(cf.cam_rot, np.array([0, 0, 150]) + cf.cam_trans)

    a = Arrow3D([co[0], xp[0]], [co[1], xp[1]], [co[2], xp[2]],
                mutation_scale=20, lw=1, arrowstyle="-|>", color="b")
    ax.add_artist(a)
    ax.text(xp[0], xp[1], xp[2], "x'", None)
    a = Arrow3D([co[0], yp[0]], [co[1], yp[1]], [co[2], yp[2]],
                mutation_scale=20, lw=1, arrowstyle="-|>", color="b")
    ax.add_artist(a)
    ax.text(yp[0], yp[1], yp[2], "y'", None)
    a = Arrow3D([co[0], zp[0]], [co[1], zp[1]], [co[2], zp[2]],
                mutation_scale=20, lw=1, arrowstyle="-|>", color="b")
    ax.add_artist(a)
    ax.text(zp[0], zp[1], zp[2], "z'", None)

    # projection line
    pp = np.dot(cf.cam_rot, np.array([0, 0, cf.sid]) + cf.cam_trans)
    a = Arrow3D([co[0], pp[0]], [co[1], pp[1]], [co[2], pp[2]],
                mutation_scale=20, lw=.7, arrowstyle="-", color="r")
    ax.add_artist(a)

    # image coordinates
    U = np.dot(cf.cam_rot, np.array([150, 0, cf.sid]) + cf.cam_trans)
    V = np.dot(cf.cam_rot, np.array([0, 150, cf.sid]) + cf.cam_trans)

    a = Arrow3D([pp[0], U[0]], [pp[1], U[1]], [pp[2], U[2]],
                mutation_scale=20, lw=1, arrowstyle="-|>", color="g")
    ax.add_artist(a)
    ax.text(U[0], U[1], U[2], 'U', None)
    a = Arrow3D([pp[0], V[0]], [pp[1], V[1]], [pp[2], V[2]],
                mutation_scale=20, lw=1, arrowstyle="-|>", color="g")
    ax.add_artist(a)
    ax.text(V[0], V[1], V[2], 'V', None)

    # TODO:  Add Projection, volume box

    # #draw cube
    # r = [-1, 1]
    # for s, e in combinations(np.array(list(product(r,r,r))), 2):
    #     if np.sum(np.abs(s-e)) == r[1]-r[0]:
    #         ax.plot3D(*zip(s,e), color="b")

    # #draw sphere
    # u, v = np.mgrid[0:2*np.pi:20j, 0:np.pi:10j]
    # x=np.cos(u)*np.sin(v)
    # y=np.sin(u)*np.sin(v)
    # z=np.cos(v)
    # ax.plot_wireframe(x, y, z, color="r")

    # #draw a point
    # ax.scatter([0],[0],[0],color="g",s=100)

    plt.show()


def MakeProjectionMovie(outdir, cfglist, projlist, vol, name='movie', fps=16):
    from mayavi import mlab     # could fail otherwise

    if outdir[-1] != '/':
        outdir += '/'
    # remove previous directory if it exists
    try:
        shutil.rmtree(outdir + 'movies/' + name)
    except OSError:
        pass
    # try making new directoryif it doesn't exist
    try:
        os.makedirs(outdir + 'movies/' + name)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST:
            pass
        else:
            raise

    # use the camera coordinates of the original figure
    mlab.figure(size=(600, 525))
    ViewProjection(cfglist[0], projlist[0], vol)
    v = mlab.view()
    for i in xrange(len(cfglist)):
        ViewProjection(cfglist[i], projlist[i], vol)
        mlab.view(*v)
        mlab.savefig(outdir + 'movies/' + name + '/frame' +
                     str(i).zfill(3) + '.png')

    devnull = open('/dev/null', 'w')
    cmd = "mencoder mf://" + outdir + "movies/" + name + "/*.png -mf" + \
        " fps=" + str(fps) + " -o " + outdir + '/' + name + ".avi" + \
        " -ovc lavc -lavcopts vcodec=mpeg4 -oac copy"
    # os.system(cmd)
    call(cmd, shell=True, stdout=devnull, stderr=devnull)
    print 'Wrote video ' + outdir + name + '.avi'
