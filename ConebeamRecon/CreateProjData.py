'''Creates sample projection data based on cone-beam projections of
artificial data

This module requires ConebeamProjection'''

import copy

import sys
# import time
import numpy as np
from scipy import linalg
from scipy.ndimage.filters import gaussian_filter
from numbers import Number

import PyCA.Core as ca
import ConebeamProjection as cp
from AppUtils import Config
# from ConebeamRecon import ReconConfig

import PyCACalebExtras.Common as cc
import PyCACalebExtras.Display as cd
Number = (Number, np.number)


def SetupScan(recon_cfg):
    '''Recon_cfg should be a Recon config file and should at least
    have the following attributes: ScanParam.numProj,
    ScanParam.scanRange, and ScanParam.SID.  SetupScan returns

    (cfglist_given, cfglist_true), lists of conebeam projection config
    files

    For isocentric, this is a setup for a simple scan, and currently
    only sets up a scan that is semi-circular with a radius of SID/2,
    and starts at the +x axis and rotates CCW around the +z axis

    For tomographic, the x-ray source follows a straight line
    '''
    # validate config (for ScanParam, at least)
    if recon_cfg.ScanParam.numProj is None:
        raise Exception("numProj is not defined in the Recon Config file")
    if recon_cfg.ScanParam.scanRange is None:
        raise Exception("scanRange is not defined in the Recon Config file")
    if recon_cfg.ScanParam.SID is None:
        raise Exception("SID is not defined in the Recon Config file")

    if recon_cfg.ScanParam.scanType is None:
        recon_cfg.ScanParam.scanType = 'isocentric'

    if recon_cfg.ScanParam.scanType.lower() in ('iso', 'isocentric'):
        return SetupScanIsocentric(recon_cfg)
    elif recon_cfg.ScanParam.scanType.lower() in ('tomo', 'tomographic'):
        return SetupScanTomographic(recon_cfg)
    else:
        raise NotImplementedError("unknown scanType. Use 'tomo' or 'iso'")


def SetupScanTomographic(recon_cfg):
    '''sets up a tomographic scan'''
    cfglist_given = []
    cfglist_true = []

    N = recon_cfg.ScanParam.numProj
    scanRange = np.pi/180*recon_cfg.ScanParam.scanRange  # in radians
    sid = recon_cfg.ScanParam.SID
    oid = recon_cfg.ScanParam.OID
    rot_base = np.array([[0, 0, -1], # starting at the +x axis
                         [1, 0, 0],
                         [0, 1, 0]])

    cfbase = cp.ConebeamProjectionConfigSpecs
    # iterate amoung the degrees
    for theta in np.linspace(-scanRange/2, scanRange/2, N):

        cam_rot = np.identity(3)
        cam_trans = np.array([sid*np.sin(theta), 0, -(sid-oid)])

        cf_given = Config.SpecToConfig(cfbase)
        cf_given.cam_trans = cam_trans
        cf_given.cam_rot = np.dot(cam_rot, rot_base)
        cf_given.piercing_point = np.array([cam_trans[0], 0.0])
        cf_given.sid = sid

        cf_true = Config.SpecToConfig(cfbase)
        cf_true = Config.SpecToConfig(cfbase)
        cf_true.cam_trans = cam_trans
        cf_true.cam_rot = np.dot(cam_rot, rot_base)
        cf_true.piercing_point = np.array([cam_trans[0], 0.0])
        cf_true.sid = sid

        cfglist_given.append(cf_given)
        cfglist_true.append(cf_given)

        # print cam_trans

    return cfglist_given, cfglist_true


def SetupScanIsocentric(recon_cfg):
    '''sets up a typical isocentric scan'''
    np.random.seed(3851684)

    cfglist_given = []
    cfglist_true = []

    # Projection trajectory
    num_proj = recon_cfg.ScanParam.numProj
    last_proj = np.pi*recon_cfg.ScanParam.scanRange/180 # convert to radians
    sid = recon_cfg.ScanParam.SID
    Tsigma = recon_cfg.ScanParam.Tsigma
    Rsigma = recon_cfg.ScanParam.Rsigma
    PPsigma = recon_cfg.ScanParam.PPsigma
    SIDsigma = recon_cfg.ScanParam.SIDsigma
    Thetasigma = recon_cfg.ScanParam.Thetasigma
    Noise = recon_cfg.ScanParam.ParamNoise
    ZeroMeanNoise = recon_cfg.ScanParam.ZeroMeanNoise
    R = sid/2                         # projection Radius
    rot_base = np.array([[0, 0, -1], # starting at the +x axis
                         [1, 0, 0],
                         [0, 1, 0]])
    # rot_base = np.array([[-1, 0, 0], # TESTING
    #                      [0, 0, -1],
    #                      [0, 1, 0]])

    # if correlated, amplify because we will smooth the vector
    if Noise == 'Correlated':
        sigma = 5.0             # for smoothing
        if Tsigma is not None:
            Tsigma *= 3.0
        if Rsigma is not None:
            Rsigma *= 6.0
        if PPsigma is not None:
            PPsigma *= 3.0
        if SIDsigma is not None:
            SIDsigma *= 3.0

    # Create noise vectors
    if Tsigma is not None:
        if isinstance(Tsigma, Number):
            Tsigma = [Tsigma*1.0]*3
        noise_x = np.zeros(num_proj)
        noise_y = np.zeros(num_proj)
        noise_z = np.zeros(num_proj)
        for i, noise in enumerate([noise_x, noise_y, noise_z]):
            if Tsigma[i] > 0:
                noise += np.random.normal(np.zeros(num_proj), Tsigma[i])
            if Noise == 'Correlated': # filter noise vectors
                noise = gaussian_filter(noise, sigma, mode='reflect')
            if ZeroMeanNoise:
                noise = noise - np.mean(noise)

    if Rsigma is not None:

        if isinstance(Rsigma, Number):
            Rsigma = [Rsigma*1.0]*3
        noise_i = np.zeros(num_proj)
        noise_j = np.zeros(num_proj)
        noise_k = np.zeros(num_proj)
        for i, noise in enumerate([noise_i, noise_j, noise_k]):
            if Rsigma[i] > 0:
                noise += np.random.normal(np.zeros(num_proj), Rsigma[i]*np.pi/360)
            if Noise == 'Correlated': # filter noise vectors
                noise[:] = gaussian_filter(noise*2, sigma*2, mode='reflect')
            if ZeroMeanNoise:
                noise[:] = noise - np.mean(noise)

    if PPsigma is not None:
        noise_u = np.random.normal(np.zeros(num_proj), PPsigma)
        noise_v = np.random.normal(np.zeros(num_proj), PPsigma)
        if Noise == 'Correlated':
            noise_u = gaussian_filter(noise_u, sigma, mode='reflect')
            noise_v = gaussian_filter(noise_v, sigma, mode='reflect')
        if ZeroMeanNoise:
            noise_u = noise_u - np.mean(noise_u)
            noise_v = noise_v - np.mean(noise_v)

    if SIDsigma is not None:
        noise_f = np.random.normal(np.zeros(num_proj), SIDsigma)
        if Noise == 'Correlated':
            noise_f = gaussian_filter(noise_f, sigma, mode='reflect')
        if ZeroMeanNoise:
            noise_f = noise_f - np.mean(noise_f)

    if Thetasigma is not None:
        sigma = 5.0             # smoothing sigma
        theta_noise = np.random.normal(np.zeros(num_proj), Thetasigma)
        if Noise == 'Correlated':
            theta_noise = gaussian_filter(theta_noise, sigma, mode='constant')
        if ZeroMeanNoise:
            theta_noise = theta_noise - np.mean(theta_noise)

    cfbase = cp.ConebeamProjectionConfigSpecs
    for i in xrange(num_proj):
        if Thetasigma is not None:
            theta_true = i*1.0/num_proj*last_proj + theta_noise[i]
            # theta_noise[0] = 0.012      # 2D test
            # theta_true = theta_noise[i] # 2D test

        # theta = 0           # test
        theta = i*1.0/num_proj*last_proj
        trans = np.array([0.0, 0.0, -R])
        cam_rot = np.array([[np.cos(theta), -np.sin(theta), 0],
                            [np.sin(theta), np.cos(theta), 0],
                            [0, 0, 1]])
        pp = np.zeros(2)

        cf_given = Config.SpecToConfig(cfbase)
        cf_given.cam_trans = trans
        cf_given.cam_rot = np.dot(cam_rot, rot_base)
        cf_given.piercing_point = pp
        cf_given.sid = sid

        cfglist_given.append(cf_given)

        # add applicable noise vectors
        if Tsigma > 0:
            trans_true = trans + np.array([noise_x[i], noise_y[i], noise_z[i]])
        elif Thetasigma > 0:
            trans_true = np.array([R*np.cos(theta_true), R*np.sin(theta_true), 0])
            # trans_true = np.array([R, R*np.sin(theta_true), 0]) # 2Dtest
        else:
            trans_true = copy.copy(trans)

        if Rsigma > 0:
            W = np.array([[0, -noise_k[i], noise_j[i]],
                          [noise_k[i], 0, -noise_i[i]],
                          [-noise_j[i], noise_i[i], 0]])
            W = linalg.expm(W)
            cam_rot_true = np.dot(W, cam_rot)

        elif Thetasigma > 0:
            cam_rot_true = np.array([[np.cos(theta_true), -np.sin(theta_true), 0],
                                     [np.sin(theta_true), np.cos(theta_true), 0],
                                     [0, 0, 1]])

        else:
            cam_rot_true = copy.copy(cam_rot)

        if PPsigma > 0:
            pp_true = pp + np.array([noise_u[i], noise_v[i]])
            # pp_true = pp + np.array([noise_u[i], 0])
            # pp_true = pp + np.array([0, noise_v[i]])
        else:
            pp_true = copy.copy(pp)

        if SIDsigma > 0:
            sid_true = sid + noise_f[i]
        else:
            sid_true = copy.copy(sid)

        cf_true = Config.SpecToConfig(cfbase)
        cf_true.cam_trans = trans_true
        cf_true.cam_rot = np.dot(cam_rot_true, rot_base)
        cf_true.piercing_point = pp_true
        cf_true.sid = sid_true

        # testing old-cs R only rotation
        # trans = np.array([R*np.cos(theta), R*np.sin(theta), 0]) # testing
        # cf_true.cam_trans = np.dot(cf_true.cam_rot.T, trans) # testing
        # cf_true.cam_trans = np.array([-np.sin(theta_true)*R, 0, -R]) # 2D test

        cfglist_true.append(cf_true)

    return (cfglist_given, cfglist_true)


def ProjectData(cfglist, cfscan=None, projgrid=None, vol=None, gain=None):
    '''given a list of pose config files along with a projection grid,
    ProjectData returns a list of projection Image3Ds

    optional parameters:
       cfscan: scan configuration
       projgrid: grid of projection data
       vol: true volume
       gain: multiplier for poisson noise (higher = less noise, None = no noise)

    if projgrid or vol or gain isn't specified, it will use the value from cfscan

    '''
    np.random.seed(389547)

    # load volume
    if vol is None:
        vol = cc.LoadMHA(cfscan.InputFile)

    if projgrid is None:
        # create grid for projections
        if len(cfscan.ScanParam.ProjSize) == 2:
            cfscan.ScanParam.ProjSize += [1]
        imsz = ca.Vec3Di(*cfscan.ScanParam.ProjSize)
        if cfscan.ScanParam.ProjSpacing is None:
            imsp = ca.Vec3Df(1.0, 1.0, 1.0)
        else:
            imsp = ca.Vec3Df(*cfscan.ScanParam.ProjSpacing)
        if cfscan.ScanParam.ProjOrigin is None:
            imor = ca.Vec3Df(-(imsz.x-1)/2.0*imsp.x,
                             -(imsz.y-1)/2.0*imsp.y, 0)
        else:
            imor = ca.Vec3Df(*cfscan.ScanParam.ProjOrigin)
        projgrid = ca.GridInfo(imsz, imsp, imor)
    else:
        cfscan.ScanParam.ProjSize = projgrid.size().tolist()
        cfscan.ScanParam.ProjSpacing = projgrid.spacing().tolist()
        cfscan.ScanParam.ProjOrigin = projgrid.origin().tolist()

    print projgrid
    projlist = [ca.Image3D(projgrid, ca.MEM_DEVICE) for _ in cfglist]

    for i, cf in enumerate(cfglist):
        # cp.ConebeamProjectFast(projlist[i], vol, cf)
        cp.ConebeamProject(projlist[i], vol, cf)

    # # this is for poisson noise
    if gain:
        print "Warning - using old poisson noise"
        cfscan.ScanParam.ImNoise = 'old poisson gain: {}'.format(gain)
        for proj in projlist:
            proj *= gain
            oldType = proj.memType()
            proj.toType(ca.MEM_HOST)
            projnp = proj.asnp()
            projnoise = np.random.poisson(projnp)
            projnp[:] = projnoise
            proj.toType(oldType)
            proj /= gain

    if cfscan.ScanParam.ImNoise:
        # divval = 12             # gives realistic mid-point x-ray for skull...
        divval = 35             # gives realistic mid-point x-ray for skull...
        I0 = cfscan.ScanParam.ImNoise
        for i, proj in enumerate(projlist):
            # projorig = proj.copy()
            # cd.DispImage(projorig, title='orig', colorbar=True, axis='cart')
            proj /= divval        # for skull data...
            proj *= -1
            ca.Exp_I(proj)
            proj *= I0
            # print ca.MinMax(proj)
            oldType = proj.memType()
            proj.toType(ca.MEM_HOST)
            projnp = proj.asnp()
            projnoise = np.random.poisson(projnp)
            projnp[:] = projnoise
            cc.SetRegionLTE(proj, proj, 1, 1)  # don't allow 0 photon counts
            cc.SetRegionGTE(proj, proj, I0, I0)  # dont allow negative atten
            # if i == 100:
            #     cc.WritePNG(proj, 'ProjPoisson', rng='imrange', axis='cart')
            # cd.DispImage(proj, title='mid', colorbar=True, axis='cart', rng=[0, I0])
            # print ca.MinMax(proj)
            proj /= I0
            ca.Log_I(proj)
            proj *= -1
            proj.toType(oldType)
            proj *= divval
            # if i == 100:
            #     cc.WritePNG(proj, 'ProjLogPoisson', rng='imrange', axis='cart')
            #     print 'wrote projlogpoisson'
            #     sys.exit()
            # cd.DispImage(proj, title='new', colorbar=True, axis='cart')
            # cd.DispImage(projorig-proj, title='new', colorbar=True, axis='cart')
    return projlist


def main():
    '''main testing function'''
    pass
