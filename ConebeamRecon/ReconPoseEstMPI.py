'''Performs an Ordered Subset Reconstruction of conebeam data while
performing a pose estimation of the data

This module requires ConebeamProjection'''
from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

import time
import PyCA.Core as ca
import ConebeamProjection as cp
import sys
# import pycuda.driver as cuda

import PyCACalebExtras.Common as cc
import PyCACalebExtras.Display as cd
import numpy as np
from numpy.linalg import norm
import matplotlib.pyplot as plt
plt.ion() # tell it to use interactive mode -- see results immediately
import copy
# from scipy.optimize.linesearch import line_search_armijo as armijo
from scipy.optimize import brent
import ReconReport


def is_number(s):
    '''check if someting is a single number'''
    try:
        float(s)
        return True
    except (ValueError, TypeError):
        return False


def RunRecon(volest, projcfglist, projlist, cf, Disp=True, report=None, saveIters=False):
    ''' Runs a Reconstruction given an Image3D volest for the
    reconstructed volume, a projection config list, a projection list,
    and a ReconConfig Config File. The projection config list configs
    are modified with the new, estimated parameters (currently only
    cam_rot and cam_trans are modified)

    Can optionally include a report object from ReconReport
    '''
    t = time.time()             # start recording the time, for report

    N = len(projlist)               # number of projections

    # slice memory manager
    ca.ThreadMemoryManager.init(volest.grid, ca.MEM_DEVICE, 1)
    projmm = ca.MemoryManager(projlist[0].grid(), ca.MEM_DEVICE, 3)

    # OSEM Parameters, get from config
    numOS = cf.OSEM.numOS
    lmda = cf.OSEM.TVweight
    numIters = cf.Recon.numIters
    EstVol = cf.Recon.EstVol
    eps = 1.e-5                 # for safe division in OSEM
    if not EstVol:
        Disp=False
        saveIters=False         # just in case

    # Recon Parameters, get from config
    TmaxPert = np.array(cf.PoseEst.TmaxPert, float) # need this b/c list comparison doesn't work
    RmaxPert = np.array(cf.PoseEst.RmaxPert, float)
    PPmaxPert = np.array(cf.PoseEst.PPmaxPert, float)
    SIDmaxPert = float(cf.PoseEst.SIDmaxPert)
    EstEvery = cf.PoseEst.EstEvery
    EstMethod = cf.PoseEst.EstMethod
    LineSearchMethod = cf.PoseEst.LineSearchMethod
    # EstIters = cf.PoseEst.EstIters # might not use
    OutputEvery = cf.OutputEvery
    if OutputEvery is None:
        OutputEvery = numIters  # don't output before end

    # give warning if Tstep and Tmaxpert are both set
    if cf.PoseEst.Tstep is not None and norm(TmaxPert) > 0:
        print "Warning! TmaxPert ignored!"
    if cf.PoseEst.Rstep is not None and norm(RmaxPert) > 0:
        print "Warning! RmaxPert ignored!"
    if cf.PoseEst.PPstep is not None and norm(PPmaxPert) > 0:
        print "Warning! PPmaxPert ignored!"
    if cf.PoseEst.SIDstep is not None and SIDmaxPert > 0:
        print "Warning! SIDmaxPert ignored!"

    # subsets is a list of subset lists
    subsets = [range(iOS, N, numOS) for iOS in range(numOS)]

    if EstVol:
        ca.SetMem(volest, 0.01)          # Initial guess for volume

    # don't bother getting the pose gradient if steps are all 0

    GetTestGradient = any((norm(TmaxPert) > 0, norm(RmaxPert) > 0, norm(PPmaxPert) > 0, SIDmaxPert > 0))
    # Get Grad magnitudes right before you start estimating (to determine initial step size)
    # However, don't estimate on iteation 0, where the volume is (usually) constant
    testGradIter = max(EstEvery - 1, cf.PoseEst.TestGradIter)

    maxGradT = 0.0
    maxGradR = 0.0
    maxGradPP = 0.0
    maxGradSID = 0.0
    Tstep = np.array([0.0, 0.0, 0.0])
    Rstep = np.array([0.0, 0.0, 0.0])
    PPstep = np.array([0.0, 0.0])
    SIDstep = 0.0

    if report is not None:
        x_trans = np.zeros((N, numIters ))
        y_trans = np.zeros((N, numIters ))
        z_trans = np.zeros((N, numIters ))
        u_time = np.zeros((N, numIters ))
        v_time = np.zeros((N, numIters ))
        sid_time = np.zeros((N, numIters ))
        R_gradmag = np.zeros((N, numIters ))
        R_time = np.zeros((N, numIters, 3, 3))
        energy = np.zeros((N, numIters ))

    # data structures for CG
    delxprev = np.zeros((9, N))  # \Delta x_{n-1}, for CG
    sprev = np.zeros((9, N))  # s_{n-1}, for CG
    # If we can't find a suitable step in the gradient direction, don't keep trying
    EstProjPose = [True] * N      # start with estimating every projection parameters
    # EstProjPose[1] = False        # Testing!!!!!!!!!!!!!

    for iter in xrange(numIters):
        print 'iter', iter
        # iterate through all subsets

        if iter == testGradIter:
            if GetTestGradient:
                print 'testing gradients'
                # Find maximum gradients, split amongst threads
                local_N = splitlist(range(N), size)[rank] # MPI
                # local_N = range(N) # if not splitting

                for k in local_N:
                    # Get Max gradient

                    grads = cp.ConebeamPoseGradient(projlist[k], volest, projcfglist[k])
                    # translate to [x', y', z']
                    g = grads[0:3]
                    if is_number(TmaxPert):
                        maxGradT = np.maximum(maxGradT, norm(grads[0:3])) # maxGradT is a scalar
                    else:
                        maxGradT = np.maximum(maxGradT, abs(g)) # maxGradT is an vector, smart gradient
                    if is_number(RmaxPert):
                        maxGradR = max(maxGradR, norm(grads[3:6]))       # maxGradR is a scalar
                    else:
                        maxGradR = np.maximum(maxGradR, abs(grads[3:6])) # maxGradR is an vector, smart gradient
                    maxGradPP = np.maximum(maxGradPP, abs(grads[6:8])) # maxGradPP is a vector, smart gradient
                    # maxGradPP = max(maxGradPP, norm(grads[6:8]))       # maxGradPP is a scalar
                    # maxGradSID = np.maximum(maxGradSID, abs(grads[8]))
                    maxGradSID = max(maxGradSID, abs(grads[8]))

                # Combine all Maxes (MPI)
                maxGradT = np.atleast_1d(maxGradT)
                maxGradR = np.atleast_1d(maxGradR)
                maxGradSID = np.atleast_1d(maxGradSID)
                szT, szR = maxGradT.size, maxGradR.size
                npmaxGrads = np.concatenate((maxGradT, maxGradR, maxGradPP, maxGradSID))
                comm.Allreduce(MPI.IN_PLACE, npmaxGrads, op=MPI.MAX)
                maxGradT = npmaxGrads[0:szT]
                maxGradR = npmaxGrads[szT:szT+szR]
                maxGradPP = npmaxGrads[szT+szR:szT+szR+2]
                maxGradSID = npmaxGrads[szT+szR+2:szT+szR+3]

                Tstep = TmaxPert/maxGradT
                Rstep = RmaxPert/maxGradR
                PPstep = PPmaxPert/maxGradPP
                SIDstep = SIDmaxPert/maxGradSID

            else:
                print 'not testing gradients'
                if cf.PoseEst.Tstep is not None:
                    Tstep = np.array(cf.PoseEst.Tstep, np.float64)
                if cf.PoseEst.Rstep is not None:
                    Rstep = np.array(cf.PoseEst.Rstep, np.float64)
                if cf.PoseEst.PPstep is not None:
                    PPstep = np.array(cf.PoseEst.PPstep, np.float64)
                if cf.PoseEst.SIDstep is not None:
                    SIDstep = np.array(cf.PoseEst.SIDstep, np.float64)

            if norm(Tstep) > 0:
                print 'Tstep:', Tstep
            if norm(Rstep) > 0:
                print 'Rstep:', Rstep
            if SIDstep > 0:
                print 'SIDstep:', SIDstep
            if norm(PPstep) > 0:
                print 'PPstep:', PPstep

        # estimate pose for all projections (after max gradients are calculated)
        GetPoseGradient = any((norm(Tstep)>0, norm(Rstep)>0, norm(PPstep)>0, SIDstep>0))
        EstPoseNow = iter > testGradIter and GetPoseGradient and iter % EstEvery == 0
        if EstMethod == 'GD':   # Gradient Descent
            # go through each subset
            for subset in subsets:
                for k in subset:
                    if EstPoseNow:
                        # Get the gradient if you need to estimate any of the pose params
                        grads = cp.ConebeamPoseGradient(projlist[k], volest, projcfglist[k])

                        # Apply step size (possibly vector)
                        g = grads[0:3]
                        g *= Tstep                            # Tstep is a vector
                        grads[0:3] = g
                        grads[3:6] *= Rstep
                        grads[6:8] *= PPstep
                        grads[8] *= SIDstep

                        # apply update (step is already applied)
                        projcfglist[k].cam_trans -= grads[0:3]
                        if norm(RmaxPert) > 0:
                            W = vec_to_Rmat(-grads[3:6])
                            projcfglist[k].cam_rot = np.dot(W, projcfglist[k].cam_rot)
                        projcfglist[k].piercing_point -= grads[6:8]
                        projcfglist[k].sid -= grads[8]

                        if report is not None:
                            energy[k, iter] = grads[9]
                            R_gradmag[k, iter] = norm(grads[3:6])*180/np.pi

                # Run OSEM Update after a subset's parameters have been estimated
                # if EstVol:
                if EstVol and rank == 0 and cf.Recon.method == 'OSEM':
                    EMSubsetUpdate(subset, volest, projlist, projcfglist, eps, lmda)
                elif EstVol and rank == 0 and cf.Recon.method == 'OSSART':
                    SARTSubsetUpdate(subset, volest, projlist, projcfglist, cf.OSSART.step)


        elif EstMethod == 'GDLS' or EstMethod == 'CG':
            if iter == 20:      # testing for skull
                Rstep *= 20
                print "Rstep is now", Rstep

            if EstPoseNow:
                # Send current volume est to all threads MPI
                # volest.toType(ca.MEM_HOST)
                # comm.Bcast(volest.asnp(), root=0)
                # volest.toType(ca.MEM_DEVICE)

                # split indices for corresponding threads MPI
                local_list = splitlist(range(N), size)[rank]
                lN = splitlistlen(range(N), size)[rank]
                # local_list = range(N) # if not splitting
                # lN = N
                paramarray = np.zeros(15*N)
                local_paramarray = np.zeros(15*lN)

                # Estimate all poses at once (ordered subsets makes too big of a change)
                # total_ests = sum(EstProjPose) # how many projs we are about to estimate
                # total_calls = 0
                for ik, k in enumerate(local_list):
                    if not EstProjPose[k]:
                        continue

                    # Energy considers the current point to be 0
                    def Energy(v):
                        testcfg = copy.deepcopy(projcfglist[k])
                        testcfg.cam_trans = projcfglist[k].cam_trans + v[0:3]
                        if norm(Rstep) > 0: # save some computations by checking first
                            W = vec_to_Rmat(v[3:6])
                            testcfg.cam_rot = np.dot(W, projcfglist[k].cam_rot)
                        testcfg.piercing_point = projcfglist[k].piercing_point + v[6:8]
                        testcfg.sid = projcfglist[k].sid + v[8]
                        en = cp.ConebeamSumSquaredErrorFast(projlist[k], volest, testcfg)
                        # en = cp.ConebeamSumSquaredError(projlist[k], volest, testcfg)
                        return en

                    grads = cp.ConebeamPoseGradient(projlist[k], volest, projcfglist[k])
                    Curr_energy = grads[9]
                    # Curr_energy = cp.ConebeamSumSquaredErrorFast(projlist[k], volest, projcfglist[k])
                    # Apply Steps now so that grad vector is pointing in the right dir
                    g = grads[0:3]
                    # adjust g gradient for "step size"
                    g *= Tstep                            # Tstep is a vector
                    delx = np.zeros(9) # not sure if necessary
                    delx[0:3] = -g
                    delx[3:6] = -Rstep*grads[3:6]
                    delx[6:8] = -PPstep*grads[6:8]
                    delx[8] = -SIDstep*grads[8]
                    if (Tstep == 0).all():
                        grads[0:3] *= 0
                    if (Rstep == 0).all():
                        grads[3:6] *= 0
                    if (PPstep == 0).all():
                        grads[6:8] *= 0
                    if SIDstep == 0:
                        grads[8] *= 0

                    # alpha0 = 20  # first line search size
                    alpha0 = 5  # first line search size

                    if EstMethod == 'CG':
                        # # B^FR - really bad
                        # denom = np.dot(delxprev[:,k], delxprev[:,k])
                        # if abs(denom) > 1e-6:
                        #     Beta = np.dot(delx, delx)/ denom
                        # else:
                        #     Beta = 0
                        # # B^PR -
                        # denom = np.dot(delxprev[:,k], delxprev[:,k])
                        # if abs(denom) > 1e-6:
                        #     Beta = np.dot(delx, delx - delxprev[:,k]) / denom
                        # else:
                        #     Beta = 0
                        # B^HS - best
                        # denom = np.dot(sprev[:,k], delx - delxprev[:,k])
                        # if abs(denom) > 1e-8:
                        #     Beta = -np.dot(delx, delx - delxprev[:,k]) / denom
                        # else:
                        #     Beta = 0
                        # # B^DY - worst
                        denom = np.dot(sprev[:,k], delx - delxprev[:,k])
                        if abs(denom) > 1e-6:
                            Beta = np.dot(delx, delx) / denom
                        else:
                            Beta = 0

                        Beta = max(Beta, 0) # reset if need be

                        if Beta == 0 and k == 0:
                            print 'Resetting Beta'
                        sn = delx + Beta*sprev[:,k]

                        # if np.dot(grads[0:9], sn) > 0: # derivative should be negative, so reset
                        #     sn = delx
                        #     Beta = 0

                        delxprev[:,k] = delx
                        sprev[:,k] = sn
                    elif EstMethod == 'GDLS':
                        sn = delx
                        Beta = 0 # just used for stopping criteria

                    if LineSearchMethod == 'brent':
                        def Energy1(alpha): # need a 1 argument energy function
                            return Energy(sn*alpha)
                        out = brent(Energy1, (), brack=[0,alpha0], full_output=True, tol=.01, maxiter=8)
                        # out = brent(Energy1, (), brack=[0,alpha0], full_output=True, tol=.01, maxiter=120)
                        alpha, f_val_at_alpha, iters, f_count = out
                        # total_calls += f_count
                    elif  LineSearchMethod == 'armijo':
                        # out = line_search_armijo(Energy, np.zeros(9), sn, grads[0:9],
                        #                          Curr_energy, alpha0=alpha0, c1 = 1e-4)
                        out = line_search_armijo(Energy, np.zeros(9), sn,
                                                 grads[0:9], Curr_energy, alpha0=alpha0, c1 = 1e-4)
                        alpha, f_count, f_val_at_alpha = out
                        if alpha == None:
                            alpha = 0
                        # total_calls += f_count
                    else:
                        raise Exception("Unknown LineSearchMethod")

                    # stop if steps are too small (be stricter for rotation steps)
                    if max(abs(sn[3:6]*alpha)) < 1e-5 and max(abs(sn*alpha)) < .01 and Beta == 0:
                        if not EstVol:
                            EstProjPose[k] = False # stop estimating
                            print "no longer updating projection ", k, \
                                "still updating ", str(sum(EstProjPose)) + '/' + str(N)
                    projcfglist[k].cam_trans += sn[0:3]*alpha          # apply update
                    if norm(Rstep) > 0:
                        W = vec_to_Rmat(sn[3:6]*alpha)
                        projcfglist[k].cam_rot = np.dot(W, projcfglist[k].cam_rot)
                    projcfglist[k].piercing_point += sn[6:8]*alpha
                    projcfglist[k].sid += sn[8]*alpha

                    if report is not None:
                        R_gradmag[k, iter] = norm(sn*alpha)*180/np.pi
                        energy[k, iter] = Curr_energy

                    # Save Params to numpy array
                    local_paramarray[ik*15:ik*15+3] = projcfglist[k].cam_trans
                    local_paramarray[ik*15+3:ik*15+12] = projcfglist[k].cam_rot.ravel()
                    local_paramarray[ik*15+12:ik*15+14] = projcfglist[k].piercing_point
                    local_paramarray[ik*15+14] = projcfglist[k].sid

                # Everybody send paramarray back MPI
                sendcounts = [15*_ for _ in splitlistlen(range(N), size)]
                displacements = [15*_[0] for _ in splitlist(range(N), size)]
                # split indices for corresponding threads MPI
                comm.Allgatherv(local_paramarray,
                                [paramarray, sendcounts, displacements, MPI.DOUBLE])
                # paramarray = local_paramarray # in case not splitting
                # Now change all thread's params from the big array
                for k in xrange(N):
                    projcfglist[k].cam_trans = paramarray[k*15:k*15+3]
                    projcfglist[k].cam_rot = paramarray[k*15+3:k*15+12].reshape(3, 3)
                    projcfglist[k].piercing_point = paramarray[k*15+12:k*15+14]
                    projcfglist[k].sid = paramarray[k*15+14]

            # even if we don't est pose, do OSEM
            # if EstVol and rank==0: # if broadcasting
            if EstVol:
                for subset in subsets:
                    EMSubsetUpdate(subset, volest, projlist, projcfglist, eps, lmda)
        else:
            raise Exception("Unknown EstMethod")

        # TESTING: Get the current estimates of all the parameters
        if report is not None and rank == 0:
            for k in xrange(N):
                # fill in following estimates (that remain the same)
                for l in xrange(iter, min(iter+EstEvery, numIters)):
                    x_trans[k, l] = projcfglist[k].cam_trans[0]
                    y_trans[k, l] = projcfglist[k].cam_trans[1]
                    z_trans[k, l] = projcfglist[k].cam_trans[2]
                    u_time[k, l] = projcfglist[k].piercing_point[0]
                    v_time[k, l] = projcfglist[k].piercing_point[1]
                    sid_time[k, l] = projcfglist[k].sid
                    R_time[k, l, :, :] = projcfglist[k].cam_rot
            # Write Report
            if (iter % OutputEvery == 0 and iter > 0) or iter == numIters-1:
                report.volest = volest
                report.run_time = time.time()-t
                report.cfglist = projcfglist
                report.x_trans = x_trans[:, :iter] # only fill in estimated iters
                report.y_trans = y_trans[:, :iter]
                report.z_trans = z_trans[:, :iter]
                report.R_gradmag = R_gradmag[:, :iter]
                report.energy = energy[:, :iter]
                report.u_time = u_time[:, :iter]
                report.v_time = v_time[:, :iter]
                report.sid_time = sid_time[:, :iter]
                report.R_time = R_time[:, :iter, :, :]

                if iter == numIters-1: # save GPU memory
                    for proj in projlist:
                        proj.toType(ca.MEM_HOST)

                ReconReport.SaveReport(report)
                ReconReport.MakeReport(report)
                if EstVol:
                    cc.WriteMHA(volest, cf.OutputFile)

                if iter == numIters-1: # save GPU memory
                    for proj in projlist:
                        proj.toType(ca.MEM_DEVICE)

        if Disp and rank == 0:                # displays the estimate each iteration
            print 'Range of estimated volume: ', ca.MinMax(volest)
            cd.DispImage(volest, newFig=False)
        if saveIters == True and rank == 0:
            cc.WritePNG(volest, cf.OutputFile + '/iter_images/iter' + str(iter).zfill(3) + '.png', rng=[0,1])


def vec_to_Rmat(v):
    '''takes a rotation gradient v (that maps via the star operator to a
    skew symmetric matrix) and a step (for how long along the geodesic
    to travel) and returns the equivalent rotation matrix that should be
    multiplied by the parameter rotation matrix

    '''
    v = v.astype(np.float64) # change to double
    vmag = norm(v)
    if vmag < 1e-10:            # too small
        return np.identity(3)
    v /= vmag
    vvt = np.outer(v, v)
    starv = np.array([[0, -v[2], v[1]],
                      [v[2], 0, -v[0]],
                      [-v[1], v[0], 0]])
    W = np.identity(3) + np.sin(vmag)*starv + \
        (1-np.cos(vmag))*(vvt - np.identity(3))
    return W


def EMSubsetUpdate(subset, volest, projlist, projcfglist, eps, lmda, projmm):
    '''given a subset (list of ints), perform the EM update'''

    # Since our background condition for BP is ones, P1sum is just a scalar
    voltmp = ca.ManagedImage3D(volest.grid(), volest.memType())
    dU = ca.ManagedImage3D(volest.grid(), volest.memType())
    imgrid = projlist[0].grid()
    imtmp = ca.ManagedImage3D(imgrid, volest.memType(), projmm)
    imtmp2 = ca.ManagedImage3D(imgrid, volest.memType(), projmm)
    imtmp3 = ca.ManagedImage3D(imgrid, volest.memType(), projmm)

    # local_subset = splitlist(subset, size)[rank] # MPI
    local_subset = subset       # if not splitting

    # OSEM update for images in current subset
    ca.SetMem(voltmp, 0.0)
    for k in local_subset:
        imgrid = projlist[k].grid()
        imtmp.setGrid(imgrid)
        imtmp2.setGrid(imgrid)
        imtmp3.setGrid(imgrid)

        # sum of backprojections
        cp.ConebeamProjectFast(imtmp, volest, projcfglist[k])
        cc.SetRegionLT(imtmp, imtmp, eps, eps, scratchI=imtmp2) # avoid NAN
        ca.Copy(imtmp3, projlist[k])
        cc.SetRegionLT(imtmp3, projlist[k], eps, eps, scratchI=imtmp2)
        ca.Div(imtmp2, imtmp3, imtmp) # proj / P(I)

        cp.ConebeamBackproject(voltmp, imtmp2, projcfglist[k], addResult=True)

    # # MPI
    # if size > 0:
    #     voltmp.toType(ca.MEM_HOST)
    #     comm.Allreduce(MPI.IN_PLACE, voltmp.asnp(), op=MPI.SUM)
    #     voltmp.toType(ca.MEM_DEVICE)

    P1sum = float(len(subset))

    # Re-estimate the volume
    if lmda > 0.0:
        cp.ComputeTVDerivative(dU, volest, eps) # figure out checkerboard?
        dU *= 1.0 * lmda * len(subset)**2/len(projlist) # subset based lambda
        dU += P1sum
        volest /= dU
    else:
        volest /= P1sum

    volest *= voltmp


def SARTSubsetUpdate(subset, volest, projlist, projcfglist, eps, lmda, projmm):
    '''given a subset (list of ints), perform the EM update'''

    onevol = ca.ManagedImage3D(volest.grid(), volest.memType())
    update = ca.ManagedImage3D(volest.grid(), volest.memType())
    ca.SetMem(onevol, 1.0)

    imgrid = projlist[0].grid()
    diff = ca.ManagedImage3D(imgrid, ca.MEM_DEVICE)
    simproj = ca.ManagedImage3D(imgrid, ca.MEM_DEVICE)
    oneproj = ca.ManagedImage3D(imgrid, ca.MEM_DEVICE)

    lmda *= 1.0/len(subset)

    ca.SetMem(update, 0.0)
    for k, proj in enumerate(projlist):
        cp.ConebeamProjectFast(simproj, volest, projcfglist[k])
        cp.ConebeamProjectFast(oneproj, onevol, projcfglist[k])
        ca.Sub(diff, proj, simproj)
        cc.SetRegionLT(oneproj, oneproj, 1.0, 1.0)
        diff /= oneproj
        # a bit of a shortcut here
        diff *= lmda
        cp.ConebeamBackproject(volest, diff, projcfglist[k], addResult=True)








#------------------------------------------------------------------------------
# Armijo line and scalar searches
#------------------------------------------------------------------------------
def line_search_armijo(f, xk, pk, gfk, old_fval, args=(), c1=1e-4, alpha0=1):

    """Minimize over alpha, the function ``f(xk+alpha pk)``.

    Parameters
    ----------
    f : callable
        Function to be minimized.
    xk : array_like
        Current point.
    pk : array_like
        Search direction.
    gfk : array_like
        Gradient of `f` at point `xk`.
    old_fval : float
        Value of `f` at point `xk`.
    args : tuple, optional
        Optional arguments.
    c1 : float, optional
        Value to control stopping criterion.
    alpha0 : scalar, optional
        Value of `alpha` at start of the optimization.

    Returns
    -------
    alpha
    f_count
    f_val_at_alpha

    Notes
    -----
    Uses the interpolation algorithm (Armijo backtracking) as suggested by
    Wright and Nocedal in 'Numerical Optimization', 1999, pg. 56-57

    """
    xk = np.atleast_1d(xk)
    fc = [0]

    def phi(alpha1):
        fc[0] += 1
        return f(xk + alpha1*pk, *args)

    if old_fval is None:
        phi0 = phi(0.)
    else:
        phi0 = old_fval  # compute f(xk) -- done in past loop

    derphi0 = np.dot(gfk, pk)
    if derphi0 > 0:
        # print 'derivative at 0 is positive!!'
        return None, fc[0], old_fval
    alpha, phi1 = scalar_search_armijo(phi, phi0, derphi0, c1=c1,
                                       alpha0=alpha0)
    return alpha, fc[0], phi1


def line_search_BFGS(f, xk, pk, gfk, old_fval, args=(), c1=1e-4, alpha0=1):
    """
    Compatibility wrapper for `line_search_armijo`
    """
    r = line_search_armijo(f, xk, pk, gfk, old_fval, args=args, c1=c1,
                           alpha0=alpha0)
    return r[0], r[1], 0, r[2]


def scalar_search_armijo(phi, phi0, derphi0, c1=1e-4, alpha0=1, amin=0):
    """Minimize over alpha, the function ``phi(alpha)``.

    Uses the interpolation algorithm (Armijo backtracking) as suggested by
    Wright and Nocedal in 'Numerical Optimization', 1999, pg. 56-57

    alpha > 0 is assumed to be a descent direction.

    Returns
    -------
    alpha
    phi1

    """
    phi_a0 = phi(alpha0)
    if phi_a0 <= phi0 + c1*alpha0*derphi0:
        # print 'first guess'
        # print 'in s_s_a: (a0, phi(a0), phi0, phi(0))', alpha0, phi_a0, phi0, phi(0)
        return alpha0, phi_a0

    # Otherwise compute the minimizer of a quadratic interpolant:

    alpha1 = -(derphi0) * alpha0**2 / 2.0 / (phi_a0 - phi0 - derphi0 * alpha0)
    phi_a1 = phi(alpha1)

    if (phi_a1 <= phi0 + c1*alpha1*derphi0):
        # print 'quadratic'
        return alpha1, phi_a1

    # Otherwise loop with cubic interpolation until we find an alpha which
    # satifies the first Wolfe condition (since we are backtracking, we will
    # assume that the value of alpha is not too small and satisfies the second
    # condition.
    count = 0
    while alpha1 > amin:       # we are assuming alpha>0 is a descent direction
        factor = alpha0**2 * alpha1**2 * (alpha1-alpha0)
        a = alpha0**2 * (phi_a1 - phi0 - derphi0*alpha1) - \
            alpha1**2 * (phi_a0 - phi0 - derphi0*alpha0)
        a = a / factor
        b = -alpha0**3 * (phi_a1 - phi0 - derphi0*alpha1) + \
            alpha1**3 * (phi_a0 - phi0 - derphi0*alpha0)
        b = b / factor

        alpha2 = (-b + np.sqrt(abs(b**2 - 3 * a * derphi0))) / (3.0*a)
        phi_a2 = phi(alpha2)

        if (phi_a2 <= phi0 + c1*alpha2*derphi0):
            # print 'cubic'
            return alpha2, phi_a2

        if (alpha1 - alpha2) > alpha1 / 2.0 or (1 - alpha2/alpha1) < 0.96:
            alpha2 = alpha1 / 2.0

        if count == 5:          # don't let it run too long
            print "Can't find a suitable step length"
            return None, phi_a1

        alpha0 = alpha1
        alpha1 = alpha2
        phi_a0 = phi_a1
        phi_a1 = phi_a2
        count += 1

    # Failed to find a suitable step length
    return None, phi_a1


def splitlist(l, n):
    '''Splits list evenly into n groups'''
    chuncks = [[] for _ in xrange(n)]
    idx = 0
    for i in xrange(n):
        idxend = idx + len(l)/n
        if i < len(l) % n:
            idxend += 1
        chuncks[i] = l[idx:idxend]
        idx = idxend
    return chuncks


def splitlistlen(l, n):
    '''Returns a list of sublist lengths from splitlist'''
    lens = [len(l)/n for _ in xrange(n)]
    for i in xrange(n):
        if i < len(l) % n:
            lens[i] += 1
    return lens
