from AppUtils import Config
import numpy as np
from numpy.linalg import norm
import scipy.linalg
import os, errno
from PyCACalebExtras import HTMLGenFuncs
import PyCACalebExtras.Common as cc
from ConebeamRecon import ViewScan, ReconConfig
import math
# from AffineReg import AffineReg
from PyCAApps.AffineReg import AffineReg
import PyCA.Core as ca
import PyCA.Common as common
import pickle
import sys
from scipy.linalg import logm, polar


class report(object):
    '''The ReconReport.report class holds data structures so that an
    HTML report gets made'''
    def __init__(self):
        self.cfscan = None
        self.cfglist_given = None
        self.cfglist_true = None
        self.cfglist = None
        self.volest = None
        self.voltrue = None
        self.x_trans = None
        self.y_trans = None
        self.z_trans = None
        self.R_gradmag = None
        self.R_time = None
        self.energy = None
        self.u_time = None
        self.v_time = None
        self.sid_time = None
        self.start_time = None
        self.run_time = None
        self.LL = [[], []]      # log posterior (for EM)
        self.Norm = [[], []]    # weighted norm (for sart/sirt/etc)
        self.L2 = []         # 3D error between ground truth and recon
        self.EstRng = None
        self.TrueRng = None
        self.TrueFname = None

    def init_arrays(self):
        '''initialize numpy arrays with zeros'''
        if self.cfscan.Recon.numIters is None:
            raise AttributeError("Cannot set numpy arrays without a scan config object")
        try:
            totalIters = sum(self.cfscan.Recon.numIters)
        except TypeError:
            totalIters = self.cfscan.Recon.numIters  # not a list
        if self.cfglist_given is not None:
            N = len(self.cfglist_given)
        elif self.cfglist is not None:
            N = len(self.cfglist)
        elif self.cfglist is not None:
            N = len(self.cfglist_true)
        else:
            raise AttributeError("no config lists set, cannot set numpy arrays")

        self.x_trans_full = np.zeros((N, totalIters))
        self.y_trans_full = np.zeros((N, totalIters))
        self.z_trans_full = np.zeros((N, totalIters))
        self.u_time_full = np.zeros((N, totalIters))
        self.v_time_full = np.zeros((N, totalIters))
        self.sid_time_full = np.zeros((N, totalIters))
        self.R_gradmag_full = np.zeros((N, totalIters))
        self.R_time_full = np.zeros((N, totalIters, 3, 3))
        self.energy_full = np.zeros((N, totalIters))
        self.energy_full.fill(np.nan)

    def saveReport(self):
        SaveReport(self)

    def makeReport(self):
        MakeReport(self)


def MakeReport(reportobj):
    '''make the html report out of the reportobj'''

    numIters = reportobj.cfscan.Recon.numIters

    import matplotlib
    import matplotlib.pyplot as plt
    font = {'size': 16}
    matplotlib.rc('font', **font)
    # axes = {'labelsize': 21,    # axis label
    #         'titlesize': 24,  # plot title
    #         'labelweight': 'bold'}
    axes = {'labelsize': 24,    # axis label
            'titlesize': 28,  # plot title
            'labelweight': 'bold'}
    matplotlib.rc('axes', **axes)

    widefigsize = (25, 6)
    widefigsize2 = (20, 6)
    widefigHTML = ' height="300" width="1200" '
    widefigHTML2 = ' height="300" width="1000" '
    volfigHTML = ' height="256" width="256" '

    prefix = reportobj.cfscan.OutputFile  # 'Results/folder/run'
    reconfigdir = prefix + '/'  # 'Results/folder/run/'
    htmlfigdir = './' + prefix.rpartition('/')[-1] + '/'  # './run/'

    # make the directories if they don't exist
    try:
        os.makedirs(prefix)
        # os.makedirs(prefix + '/figs')
    except OSError as exc:      # Python >2.5
        if exc.errno == errno.EEXIST:
            pass
        else:
            raise

    cfscan = reportobj.cfscan
    cfglist_given = reportobj.cfglist_given
    cfglist_true = reportobj.cfglist_true
    cfglist = reportobj.cfglist
    volest = reportobj.volest
    voltrue = reportobj.voltrue
    x_trans = reportobj.x_trans
    y_trans = reportobj.y_trans
    z_trans = reportobj.z_trans
    R_gradmag = reportobj.R_gradmag
    energy = reportobj.energy
    LL = reportobj.LL
    L2 = reportobj.L2
    Norm = reportobj.Norm
    u_time = reportobj.u_time
    v_time = reportobj.v_time
    sid_time = reportobj.sid_time
    run_time = reportobj.run_time
    EstRng = reportobj.EstRng
    TrueRng = reportobj.TrueRng
    TrueFname = reportobj.TrueFname

    def paramset(param, nIters):
        '''returns true if a nonzero/non-none value exists in it somewhere.
        Supports a list of params, where a param is a number, none,
        or a list of (3) numbers'''
        if isinstance(nIters, list) and len(nIters) == 1:
            nIters = nIters[0]
        if cc.IsNumber(nIters):
            if nIters == 0 or param is None or norm(param) == 0:
                return False
            else:
                return True
        else:
            return any(paramset(p, nIt) for p, nIt in zip(param, nIters))

    # Tstep could be None, a list of Nones, 0, or a list of zeros
    Test = (paramset(cfscan.PoseEst.TmaxPert, numIters) or
            paramset(cfscan.PoseEst.Tstep, numIters))
    Rest = (paramset(cfscan.PoseEst.RmaxPert, numIters) or
            paramset(cfscan.PoseEst.Rstep, numIters))
    PPest = (paramset(cfscan.PoseEst.PPmaxPert, numIters) or
             paramset(cfscan.PoseEst.PPstep, numIters))
    SIDest = (paramset(cfscan.PoseEst.SIDmaxPert, numIters) or
              paramset(cfscan.PoseEst.SIDstep, numIters))

    try:
        TestGradIter = reportobj.cfscan.PoseEst.TestGradIter
    except AttributeError:
        reportobj.cfscan.PoseEst.TestGradIter = 1
        TestGradIter = 1

    N = len(reportobj.cfglist)
    # numIters = reportobj.cfscan.Recon.numIters

    # Set bools for what to plot
    ShowT = Test or cfscan.ScanParam.Tsigma > 0
    if cfglist_given is not None and cfglist_true is not None:
        if np.linalg.norm(cfglist_given[0].cam_trans - cfglist_true[0].cam_trans) > 0:
            ShowT = True
        if np.linalg.norm(cfglist_given[1].cam_trans - cfglist_true[1].cam_trans) > 0:
            ShowT = True
    ShowTCam = Test
    if cfglist_given is not None and cfglist_true is not None:
        if np.linalg.norm(cfglist_given[0].cam_trans - cfglist_given[-1].cam_trans) > 0:
            ShowTCam = True
        if np.linalg.norm(cfglist_true[0].cam_trans - cfglist_true[-1].cam_trans) > 0:
            ShowTCam = True
    ShowTtime = Test
    ShowR = Rest or cfscan.ScanParam.Rsigma > 0 or cfscan.ScanParam.Thetasigma > 0
    if cfglist_given is not None and cfglist_true is not None:
        if np.linalg.norm(cfglist_given[0].cam_rot - cfglist_true[0].cam_rot) > 0:
            ShowR = True
        if np.linalg.norm(cfglist_given[1].cam_rot - cfglist_true[1].cam_rot) > 0:
            ShowR = True
    ShowRtime = Rest
    ShowPP = PPest or cfscan.ScanParam.PPsigma > 0
    if cfglist_given is not None and cfglist_true is not None:
        if np.linalg.norm(np.array(cfglist_given[0].piercing_point) -
                          np.array(cfglist_true[0].piercing_point)) > 0:
            ShowPP = True
        if np.linalg.norm(np.array(cfglist_given[1].piercing_point) -
                          np.array(cfglist_true[1].piercing_point)) > 0:
            ShowPP = True
    ShowPPtime = PPest
    ShowSID = SIDest or cfscan.ScanParam.SIDsigma > 0
    if cfglist_given is not None and cfglist_true is not None:
        if np.linalg.norm(cfglist_given[0].sid - cfglist_true[0].sid) > 0:
            ShowSID = True
        if np.linalg.norm(cfglist_given[1].sid - cfglist_true[1].sid) > 0:
            ShowSID = True
    ShowSIDtime = SIDest

    page = HTMLGenFuncs.GenHeadScript('Recon Report', defaultImScale=2.0)
    page += '<h2>Reconstruction</h2>\n'
    if run_time is not None:
        page += '<h3> Elapsed Time: %g min, %g seconds</h3>' \
                % (int(math.floor(run_time/60)), run_time % 60)

    with open(reconfigdir + 'configyaml', 'w') as f:  # for browser :(
        f.write(Config.ConfigToYAML(ReconConfig.ReconConfigSpecs, cfscan))
    with open(reconfigdir + 'config.yaml', 'w') as f:
        f.write(Config.ConfigToYAML(ReconConfig.ReconConfigSpecs, cfscan))
    page += '<p><a href="' + htmlfigdir + 'configyaml">YAML Config</a></p>\n'

    if EstRng is None:
        rng = 'imrange'
    else:
        rng = EstRng
    cc.WritePNG(volest, reconfigdir + 'Imrecon.png', rng=rng)
    cc.WritePNG(volest, reconfigdir + 'Imrecon_y.png', rng=rng, dim='y', axis='cart')
    cc.WritePNG(volest, reconfigdir + 'Imrecon_x.png', rng=rng, dim='x', axis='cart')
    page += '<img' + volfigHTML + ' src="' + htmlfigdir + 'Imrecon.png" />\n'
    page += '<img' + volfigHTML + ' src="' + htmlfigdir + 'Imrecon_y.png" />\n'
    page += '<img' + volfigHTML + ' src="' + htmlfigdir + 'Imrecon_x.png" />\n'
    if TrueFname is not None:
        page += '<br>'
        voltrue = cc.Load(reportobj.TrueFname)
        cc.WritePNG(voltrue, reconfigdir + 'Truerecon.png',
                    rng=TrueRng)
        cc.WritePNG(voltrue, reconfigdir + 'Truerecon_y.png',
                    rng=TrueRng, dim='y', axis='cart')
        cc.WritePNG(voltrue, reconfigdir + 'Truerecon_x.png',
                    rng=TrueRng, dim='x', axis='cart')
        page += '<img' + volfigHTML + ' src="' + htmlfigdir + 'Truerecon.png" />\n'
        page += '<img' + volfigHTML + ' src="' + htmlfigdir + 'Truerecon_y.png" />\n'
        page += '<img' + volfigHTML + ' src="' + htmlfigdir + 'Truerecon_x.png" />\n'

    cc.WriteMHA(volest, reconfigdir + 'Imrecon.mha')

    if cfscan.Recon.EstVol and cfscan.InputFile is not None:
        try:
            # calculate affine using downsampled images
            voltrue = cc.LoadMHA(os.path.dirname(cfscan.OutputFile) + '/realtruth.mha',
                                 ca.MEM_DEVICE)
            voltrueds = cc.DownsampleBin(voltrue, 2)
            volestds = cc.DownsampleBin(volest, 2)
            ImReg = ca.Image3D(voltrue.grid(), voltrue.memType())

            sys.stdout.write('solving for affine...')
            sys.stdout.flush()
            out = AffineReg(volestds, voltrueds, constraint='rigid',
                            plot=False, maxIter=10, autostop=False, verbose=0)
            A = out[1]
            cc.ApplyAffineReal(ImReg, volest, A)
            cc.WritePNG(ImReg, reconfigdir + 'Regz.png', dim='z')
            cc.WritePNG(ImReg, reconfigdir + 'Regy.png', dim='y', axis='cart')
            cc.WritePNG(ImReg, reconfigdir + 'Regx.png', dim='x', axis='cart')
            ImReg -= voltrue
            cc.WritePNG(ImReg, reconfigdir + 'diff_rigid.png', rng=[-.4, .4])
            sys.stdout.write('done\n')
            sys.stdout.flush()
            sys.stdout.write('solving for affine...')
            out = AffineReg(volestds, voltrueds,
                            plot=False, maxIter=10, autostop=False, verbose=0)
            A = out[1]
            cc.ApplyAffineReal(ImReg, volest, A)
            ImReg -= voltrue
            l2error = np.sqrt(ca.Sum2(ImReg))
            cc.WritePNG(ImReg, reconfigdir + 'diff_affine.png', rng=[-.4, .4])

            page += '<br>'
            page += '<img' + volfigHTML + ' src="' + htmlfigdir + 'Regz.png" />\n'
            page += '<img' + volfigHTML + ' src="' + htmlfigdir + 'Regy.png" />\n'
            page += '<img' + volfigHTML + ' src="' + htmlfigdir + 'Regx.png" />\n'
            page += '<br>'
            page += '<h3>L2 Error {}</h3>\n'.format(l2error)
            page += '<img' + volfigHTML + ' src="' + htmlfigdir + 'diff_rigid.png" />\n'
            page += '<img' + volfigHTML + ' src="' + htmlfigdir + 'diff_affine.png" />\n'
            sys.stdout.write('done\n')

        except IOError:
            print "Couldn't load 'True' Image"
            # cc.WritePNG(ImReg, reconfigdir + 'Imy.png', dim='y', axis='cart')
            # cc.WritePNG(ImReg, reconfigdir + 'Imx.png', dim='x', axis='cart')
            # page += '<img' + volfigHTML + ' src="' + htmlfigdir + 'Imy.png" />\n'
            # page += '<img' + volfigHTML + ' src="' + htmlfigdir + 'Imx.png" />\n'
        except RuntimeError:
            print "not at full scale"

    page += '<br>'

    ViewScan.ViewScan2D(cfglist_given, reportobj.cfscan, plotvol=True)
    figname = 'path_given.png'
    plt.savefig(reconfigdir + figname)
    plt.savefig(reconfigdir + figname[:-3] + 'pdf')
    # plt.savefig(reconfigdir + figname[:-3] + 'eps')
    plt.close('all')
    page += '<img height="300" width="400" src="' + htmlfigdir + figname + '" title="Given Path" />'

    if cfglist_true is not None:
        ViewScan.ViewScan2D(cfglist_true, reportobj.cfscan, plotvol=True)
        figname = '/path_true.png'
        plt.savefig(prefix + '/' + figname)
        plt.savefig(reconfigdir + figname[:-3] + 'pdf')
        # plt.savefig(reconfigdir + figname[:-3] + 'eps')
        plt.close('all')
        page += '<img height="300" width="400" src="' + htmlfigdir + figname + '" title="True Path" />'
    else:
        page += 'No Ground Truth'

    ViewScan.ViewScan2D(cfglist, reportobj.cfscan, plotvol=True)
    figname = 'path_estimated.png'
    plt.savefig(reconfigdir + figname)
    plt.savefig(reconfigdir + figname[:-3] + 'pdf')
    # plt.savefig(reconfigdir + figname[:-3] + 'eps')
    plt.close('all')
    page += '<img height="300" width="400" src="' + htmlfigdir + figname + '" title="Estimated Path" />'

    ### Translation Estimation ### (always show cam coords)
    if ShowTCam:
        page += '<h2>Translation Estimation</h2>\n'
        page += '<h3>Camera Coordinates</h3>\n'

        newvec = [np.array(i.cam_trans) for i in cfglist]
        if cfglist_true is not None:
            truevec = [np.array(i.cam_trans) for i in cfglist_true]
        origvec = [np.array(i.cam_trans) for i in cfglist_given]
        # X', Y', Z'
        for j, ax in enumerate(['x', 'y', 'z']):
            figname = 'trans_' + ax + 'p.png'
            plt.figure(figsize=widefigsize)
            plt.title(ax.upper() + "' estimate")
            new, = plt.plot([i[j] for i in newvec])
            orig, = plt.plot([i[j] for i in origvec])
            if cfglist_true is not None:
                true, = plt.plot([i[j] for i in truevec])
                plt.legend([new, orig, true], ["Estimated Translation",
                                               "Initialization",
                                               "Ground Truth Translation"])
            else:
                plt.legend([new, orig], ["Estimated Translation",
                                         "Initialization"] )
            plt.autoscale(enable=True, axis='x', tight=True)
            plt.savefig(reconfigdir + figname)
            plt.savefig(reconfigdir + figname[:-3] + 'pdf')
            # plt.savefig(reconfigdir + figname[:-3] + 'eps')
            plt.close('all')
            page += '<img ' + widefigHTML + ' src="' + htmlfigdir + figname + '" /> <br>'

            # page += '<h3>Camera Coordinates</h3>\n'
            # newvec = [np.array(i.cam_trans)-np.array(j.cam_trans) for i, j in zip(cfglist, cfglist_given)] # NewCS
            # if cfglist_true is not None:
            #     truevec = [np.array(i.cam_trans)-np.array(j.cam_trans) for i, j in zip(cfglist_true, cfglist_given)] # NewCS
            # origvec = [np.array(i.cam_trans)-np.array(j.cam_trans) for i, j in zip(cfglist_given, cfglist_given)] # NewCS
            # newvecmean = np.mean(np.array(newvec), 0)
            # if cfglist_true is not None:
            #     truevecmean = np.mean(np.array(truevec), 0)
            # origvecmean = np.mean(np.array(origvec), 0)

            # # X', Y', Z' MR
            # for j, ax in enumerate(['x', 'y', 'z']):
            #     figname = 'trans_' + ax + 'pMR.png'
            #     plt.figure(figsize=widefigsize)
            #     plt.title(ax.upper() + "' estimate (Mean Removed) ")
            #     new, = plt.plot([i[j] - newvecmean[j] for i in newvec])
            #     orig, = plt.plot([i[j] - origvecmean[j] for i in origvec])
            #     if cfglist_true is not None:
            #         true, = plt.plot([i[j] - truevecmean[j] for i in truevec])
            #         plt.legend([new, orig, true], ["Estimated Translation",
            #                                        "Initialization",
            #                                        "Ground Truth Translation"])
            #     else:
            #         plt.legend([new, orig], ["Estimated Translation",
            #                                  "Initialization"])
            #     plt.autoscale(enable=True, axis='x', tight=True)
            #     plt.savefig(reconfigdir + figname)
            #     plt.savefig(reconfigdir + figname[:-3] + 'pdf')
                # plt.savefig(reconfigdir + figname[:-3] + 'eps')
            #     plt.close('all')
            #     page += '<img ' + widefigHTML + ' src="' + htmlfigdir + figname + '" /> <br>'

    if ShowTtime:
        # Translation estimates over time
        x_transdiff = np.zeros((N, x_trans.shape[1]))
        y_transdiff = np.zeros((N, y_trans.shape[1]))
        z_transdiff = np.zeros((N, z_trans.shape[1]))
        for i in xrange(N):
            for j in xrange(x_trans.shape[1]):
                trans = (np.array([x_trans[i, j], y_trans[i, j], z_trans[i, j]]) -
                         np.array([x_trans[i, 0], y_trans[i, 0], z_trans[i, 0]]))

                x_transdiff[i, j] = trans[0] - x_transdiff[i, 0]
                y_transdiff[i, j] = trans[1] - y_transdiff[i, 0]
                z_transdiff[i, j] = trans[2] - z_transdiff[i, 0]

        # X'
        figname = 'trans_xptime.png'
        plt.figure(figsize=widefigsize)
        plt.title("X' estimate over time")
        plt.plot(x_transdiff.T)
        plt.autoscale(enable=True, axis='x', tight=True)
        plt.savefig(reconfigdir + figname)
        plt.savefig(reconfigdir + figname[:-3] + 'pdf')
        # plt.savefig(reconfigdir + figname[:-3] + 'eps')
        plt.close('all')
        page += '<img height="250" width="1041" src="' + htmlfigdir + figname + '" /> <br>'
        # Y'
        figname = 'trans_yptime.png'
        plt.figure(figsize=widefigsize)
        plt.title("Y' estimate over time")
        plt.plot(y_transdiff.T)
        plt.autoscale(enable=True, axis='x', tight=True)
        plt.savefig(reconfigdir + figname)
        plt.savefig(reconfigdir + figname[:-3] + 'pdf')
        # plt.savefig(reconfigdir + figname[:-3] + 'eps')
        plt.close('all')
        page += '<img height="250" width="1041" src="' + htmlfigdir + figname + '" /> <br>'
        # Z'
        figname = 'trans_zptime.png'
        plt.figure(figsize=widefigsize)
        plt.title("Z' estimate over time")
        plt.plot(z_transdiff.T)
        plt.autoscale(enable=True, axis='x', tight=True)
        plt.savefig(reconfigdir + figname)
        plt.savefig(reconfigdir + figname[:-3] + 'pdf')
        # plt.savefig(reconfigdir + figname[:-3] + 'eps')
        plt.close('all')
        page += '<img height="250" width="1041" src="' + htmlfigdir + figname + '" /> <br>'

    if ShowT:
        # World Coords
        page += '<h3>World Coordinates</h3>\n'
        newvec = [np.dot(j.cam_rot, np.array(i.cam_trans)-np.array(j.cam_trans))
                  for i, j in zip(cfglist, cfglist_given)]
        if cfglist_true is not None:
            truevec = [np.dot(j.cam_rot, np.array(i.cam_trans)-np.array(j.cam_trans))
                       for i, j in zip(cfglist_true, cfglist_given)]
        origvec = [np.dot(j.cam_rot, np.array(i.cam_trans)-np.array(j.cam_trans))
                   for i, j in zip(cfglist_given, cfglist_given)]

        # X, Y, Z
        for j, ax in enumerate(['x', 'y', 'z']):
            figname = 'trans_' + ax + '.png'
            plt.figure(figsize=widefigsize)
            plt.title(ax.upper() + " estimate")
            new, = plt.plot([i[j] for i in newvec])
            orig, = plt.plot([i[j] for i in origvec])
            if cfglist_true is not None:
                true, = plt.plot([i[j] for i in truevec])
                plt.legend([new, orig, true], ["Estimated Translation",
                                               "Initialization",
                                               "Ground Truth Translation"])
            else:
                plt.legend([new, orig], ["Estimated Translation",
                                         "Initialization"])
            plt.autoscale(enable=True, axis='x', tight=True)
            plt.savefig(reconfigdir + figname)
            plt.savefig(reconfigdir + figname[:-3] + 'pdf')
            # plt.savefig(reconfigdir + figname[:-3] + 'eps')
            plt.close('all')
            page += '<img ' + widefigHTML + ' src="' + htmlfigdir + figname + '" /> <br>'

        page += '<h3>World Coordinates (Mean removed)</h3>\n'
        newvec = [np.dot(j.cam_rot, np.array(i.cam_trans)-np.array(j.cam_trans)) # NewCS
                  for i, j in zip(cfglist, cfglist_given)]   # NewCS
        if cfglist_true is not None:
            truevec = [np.dot(j.cam_rot, np.array(i.cam_trans)-np.array(j.cam_trans))    # NewCS
                       for i, j in zip(cfglist_true, cfglist_given)] # NewCS
        origvec = [np.dot(j.cam_rot, np.array(i.cam_trans)-np.array(j.cam_trans)) # NewCS
                   for i, j in zip(cfglist_given, cfglist_given)] # NewCS
        # newvec = [i.cam_trans-j.cam_trans for i, j in zip(cfglist, cfglist_given)] # OldCS
        # truevec = [i.cam_trans-j.cam_trans for i, j in zip(cfglist_true, cfglist_given)] # OldCS
        # origvec = [i.cam_trans-j.cam_trans for i, j in zip(cfglist_given, cfglist_given)] # OldCS
        newvecmean = np.mean(np.array(newvec), 0)
        if cfglist_true is not None:
            truevecmean = np.mean(np.array(truevec), 0)
        origvecmean = np.mean(np.array(origvec), 0)

        # X, Y, Z  (mean removed)
        for j, ax in enumerate(['x', 'y', 'z']):
            figname = 'trans_' + ax + 'MR.png'
            plt.figure(figsize=widefigsize)
            plt.title(ax.upper() + " estimate (Mean removed)")

            new, = plt.plot([i[j] - newvecmean[j] for i in newvec])
            orig, = plt.plot([i[j] - origvecmean[j] for i in origvec])
            if cfglist_true is not None:
                true, = plt.plot([i[j] - truevecmean[j] for i in truevec])
                plt.legend([new, orig, true], ["Estimated Translation",
                                               "Initialization",
                                               "Ground Truth Translation"])
            else:
                plt.legend([new, orig], ["Estimated Translation",
                                         "Initialization"])
            plt.autoscale(enable=True, axis='x', tight=True)
            plt.savefig(reconfigdir + figname)
            plt.savefig(reconfigdir + figname[:-3] + 'pdf')
            # plt.savefig(reconfigdir + figname[:-3] + 'eps')
            plt.close('all')
            page += '<img ' + widefigHTML + ' src="' + htmlfigdir + figname + '" /> <br>'

        if cfglist_true is not None:
            # X, Y, Z  error
            for j, ax in enumerate(['x', 'y', 'z']):
                figname = 'trans_' + ax + 'error.png'
                plt.figure(figsize=widefigsize2)
                plt.title(ax.upper() + " estimate error (Mean removed)", fontsize=32)

                new, = plt.plot([abs(nv[j] - newvecmean[j] - tv[j] + truevecmean[j])
                                 for nv, tv in zip(newvec, truevec)], 'b', linewidth=2)
                orig, = plt.plot([abs(ov[j] - origvecmean[j] - tv[j] + truevecmean[j])
                                  for ov, tv in zip(origvec, truevec)], 'r', linewidth=2)
                leg = plt.legend([orig, new], ["Nominal Error",
                                               "New (estimated) Error"],
                                 fontsize=26, fancybox=True)
                leg.get_frame().set_alpha(0.5)
                plt.ylabel('Error (mm)', fontsize=28)
                plt.xlabel('Projection', fontsize=28)
                plt.autoscale(enable=True, axis='x', tight=True)
                plt.setp(plt.gca().get_xticklabels(), fontsize=20)
                plt.setp(plt.gca().get_yticklabels(), fontsize=20)

                # plt.tight_layout()
                plt.gcf().tight_layout()
                # plt.gcf().subplots_adjust(bottom=0.5)
                plt.savefig(reconfigdir + figname)
                plt.savefig(reconfigdir + figname[:-3] + 'pdf')
                # plt.savefig(reconfigdir + figname[:-3] + 'eps')
                plt.close('all')
                page += '<img ' + widefigHTML2 + ' src="' + htmlfigdir + figname + '" /> <br>'

            figname = 'trans_error.png'
            plt.figure(figsize=widefigsize)
            plt.title("Translation estimate error")

            # newerr = [np.linalg.norm(nv-newvecmean -tv + truevecmean)
            #           for nv, tv in zip(newvec,truevec)]
            # olderr = [np.linalg.norm(ov-origvecmean -tv + truevecmean)
            #                   for ov, tv in zip(origvec,truevec)]
            # print 'newerr', newerr
            # print 'olderr', olderr
            # print 'newerr', np.mean(newerr)
            # print 'olderr', np.mean(olderr)
            # sys.exit()

            new, = plt.plot([np.linalg.norm(nv - newvecmean - tv + truevecmean)
                             for nv, tv in zip(newvec, truevec)], 'b', linewidth=2)
            orig, = plt.plot([np.linalg.norm(ov - origvecmean - tv + truevecmean)
                              for ov, tv in zip(origvec, truevec)], 'r', linewidth=2)
            plt.legend([orig, new], ["Nominal Error",
                                     "New (estimated) Error"])
            plt.ylabel('Error (mm)')
            plt.xlabel('Projection')
            plt.autoscale(enable=True, axis='x', tight=True)
            plt.savefig(reconfigdir + figname)
            plt.savefig(reconfigdir + figname[:-3] + 'pdf')
            # plt.savefig(reconfigdir + figname[:-3] + 'eps')
            plt.close('all')
            page += '<img ' + widefigHTML + ' src="' + htmlfigdir + figname + '" /> <br>'

    if ShowR:
        page += '<h2>Rotation Estimation</h2>\n'

        # if cfglist_true is not None:
        #     # error from ground truth
        #     figname = 'theta_groundtruth.png'
        #     plt.figure(figsize=widefigsize)
        #     plt.title("Distance from Ground Truth")
        #     olderr = [np.linalg.norm(logm(np.dot(cf1.cam_rot.T, cf2.cam_rot)))
        #               for cf1, cf2 in zip(cfglist_given, cfglist_true)]
        #     newerr = [np.linalg.norm(logm(np.dot(cf1.cam_rot.T, cf2.cam_rot)))
        #               for cf1, cf2 in zip(cfglist, cfglist_true)]
        #     new, = plt.plot(newerr, 'b')
        #     orig, = plt.plot(olderr, 'r')
        #     plt.legend([orig, new], ["Nominal Error",
        #                              "New (estimated) Error"])
        #     plt.autoscale(enable=True, axis='x', tight=True)
        #     plt.savefig(reconfigdir + figname)
        #     plt.savefig(reconfigdir + figname[:-3] + 'pdf')
        #     plt.savefig(reconfigdir + figname[:-3] + 'eps')
        #     plt.close('all')
        #     page += '<img ' + widefigHTML + ' src="' + htmlfigdir + figname + '" /> <br>'

        #     try:
        #         # error from ground truth, MR
        #         figname = 'theta_groundtruthmr.png'
        #         plt.figure(figsize=widefigsize)
        #         plt.title("Rotation Error (Mean Removed)")

        #         Rbar = np.zeros((3, 3))
        #         for i, cf1 in enumerate(cfglist_given):
        #             Mi = cfglist_true[i].cam_rot
        #             Ni = cf1.cam_rot
        #             Rbar += np.dot(Ni, Mi.T)
        #         Rbar /= N                 # average in M(3)
        #         R, _ = polar(Rbar) # projection to S0(3)
        #         olderr = [np.linalg.norm(logm(np.dot(cf1.cam_rot.T, np.dot(R, cf2.cam_rot))))
        #                   for cf1, cf2 in zip(cfglist_given, cfglist_true)]

        #         Rbar = np.zeros((3, 3))
        #         for i, cf1 in enumerate(cfglist):
        #             Mi = cfglist_true[i].cam_rot
        #             Ni = cf1.cam_rot
        #             Rbar += np.dot(Ni, Mi.T)
        #         Rbar /= N                 # average in M(3)
        #         R, _ = polar(Rbar) # projection to S0(3)
        #         newerr = [np.linalg.norm(logm(np.dot(cf1.cam_rot.T, np.dot(R, cf2.cam_rot))))
        #                   for cf1, cf2 in zip(cfglist, cfglist_true)]

        #         new, = plt.plot(newerr, 'b')
        #         orig, = plt.plot(olderr, 'r')
        #         plt.legend([orig, new], ["Nominal Error",
        #                                  "New (estimated) Error"])
        #         plt.autoscale(enable=True, axis='x', tight=True)
        #         plt.savefig(reconfigdir + figname)
        #         plt.savefig(reconfigdir + figname[:-3] + 'pdf')
        #         plt.savefig(reconfigdir + figname[:-3] + 'eps')
        #         plt.close('all')
        #         page += '<img ' + widefigHTML + ' src="' + htmlfigdir + figname + '" /> <br>'
        #     except AttributeError:
        #         pass

        #     # error from ground truth (looking at PP)
        #     figname = 'theta_asPP.png'
        #     plt.figure(figsize=widefigsize)
        #     plt.title("Rotation Error (mm)")

        #     vec_est = np.zeros((3, N))
        #     vec_given = np.zeros((3, N))
        #     vec_true = np.zeros((N, 3))
        #     for i, cft in enumerate(cfglist_true):
        #         cfg = cfglist_given[i]
        #         cfe = cfglist[i]
        #         vec_est[:, i] = np.dot(cfe.cam_rot, cfe.cam_trans)  # PP
        #         vec_given[:, i] = np.dot(cfg.cam_rot, cfg.cam_trans)
        #         vec_true[i, :] = np.dot(cft.cam_rot, cft.cam_trans)

        #     olderr = [np.linalg.norm(vec_given[:, i] - vec_true[i, :]) for i in xrange(N)]
        #     newerr = [np.linalg.norm(vec_est[:, i] - vec_true[i, :]) for i in xrange(N)]
        #     new, = plt.plot(newerr, 'b')
        #     orig, = plt.plot(olderr, 'r')
        #     plt.legend([orig, new], ["Nominal Error",
        #                              "New (estimated) Error"])
        #     plt.autoscale(enable=True, axis='x', tight=True)
        #     plt.savefig(reconfigdir + figname)
        #     plt.savefig(reconfigdir + figname[:-3] + 'pdf')
        #     plt.savefig(reconfigdir + figname[:-3] + 'eps')
        #     plt.close('all')
        #     page += '<img ' + widefigHTML + ' src="' + htmlfigdir + figname + '" /> <br>'

        #     figname = 'theta_asPP_MR.png'
        #     plt.figure(figsize=widefigsize)
        #     plt.title("Rotation Error (mean removed)")
        #     # Solve for best R for original to GT
        #     H = np.dot(vec_given, vec_true)
        #     U, S, Vt = np.linalg.svd(H)
        #     R = np.dot(Vt.T, U.T)
        #     olderr = [np.linalg.norm(np.dot(R, vec_given[:, i]) - vec_true[i, :])
        #               for i in xrange(N)]
        #     H = np.dot(vec_est, vec_true)
        #     U, S, Vt = np.linalg.svd(H)
        #     R = np.dot(Vt.T, U.T)
        #     newerr = [np.linalg.norm(np.dot(R, vec_est[:, i]) - vec_true[i, :])
        #               for i in xrange(N)]
        #     new, = plt.plot(newerr, 'b')
        #     orig, = plt.plot(olderr, 'r')
        #     plt.legend([orig, new], ["Nominal Error",
        #                              "New (estimated) Error"])
        #     plt.autoscale(enable=True, axis='x', tight=True)
        #     plt.savefig(reconfigdir + figname)
        #     plt.savefig(reconfigdir + figname[:-3] + 'pdf')
        #     plt.savefig(reconfigdir + figname[:-3] + 'eps')
        #     plt.close('all')
        #     page += '<img ' + widefigHTML + ' src="' + htmlfigdir + figname + '" /> <br>'

        # Neighbor rotation distance
        figname = 'theta_neighbor.png'
        plt.figure(figsize=widefigsize)
        plt.title("Distance from Neighbor")
        givendist = [np.linalg.norm(logm(np.dot(cf1.cam_rot.T, cf2.cam_rot)))
                     for cf1, cf2 in zip(cfglist_given[1:], cfglist_given[:-1])]
        if cfglist_true is not None:
            truedist = [np.linalg.norm(logm(np.dot(cf1.cam_rot.T, cf2.cam_rot)))
                        for cf1, cf2 in zip(cfglist_true[1:], cfglist_true[:-1])]
        estdist = [np.linalg.norm(logm(np.dot(cf1.cam_rot.T, cf2.cam_rot)))
                   for cf1, cf2 in zip(cfglist[1:], cfglist[:-1])]
        given, = plt.plot(givendist)
        est, = plt.plot(estdist)
        if cfglist_true is not None:
            true, = plt.plot(truedist)
            plt.legend([given, true, est],
                       ["Given Distance", "True Distance", "Est Distance"])
        else:
            plt.legend([given, est],
                       ["Given Distance", "Est Distance"])
        plt.autoscale(enable=True, axis='x', tight=True)
        plt.savefig(reconfigdir + figname)
        plt.savefig(reconfigdir + figname[:-3] + 'pdf')
        # plt.savefig(reconfigdir + figname[:-3] + 'eps')
        plt.close('all')
        page += '<img ' + widefigHTML + ' src="' + htmlfigdir + figname + '" /> <br>'

        # Neighbor rotation error (for report)
        if cfglist_true is not None:

            figname = 'theta_neighbor_error.png'
            plt.figure(figsize=widefigsize2)
            plt.title("Distance from Neighbor Error", fontsize=32)
            givenerror = [abs(errgiven - errtrue)*180.0/np.pi
                          for errgiven, errtrue in zip(givendist, truedist)]
            esterror = [abs(errest - errtrue)*180.0/np.pi
                        for errest, errtrue in zip(estdist, truedist)]
            given, = plt.plot(givenerror, 'r', linewidth=2)
            est, = plt.plot(esterror, 'b', linewidth=2)
            leg = plt.legend([given, est],
                             ["Nominal Error", "Estimated Error"],
                             fontsize=26,
                             fancybox=True)
            leg.get_frame().set_alpha(0.5)
            plt.ylabel('Error (deg.)', fontsize=28)
            plt.xlabel('Projection', fontsize=28)
            plt.autoscale(enable=True, axis='x', tight=True)
            plt.setp(plt.gca().get_xticklabels(), fontsize=20)
            plt.setp(plt.gca().get_yticklabels(), fontsize=20)
            plt.gcf().tight_layout()
            plt.savefig(reconfigdir + figname)
            plt.savefig(reconfigdir + figname[:-3] + 'pdf')
            # plt.savefig(reconfigdir + figname[:-3] + 'eps')
            plt.close('all')
            page += '<img ' + widefigHTML2 + ' src="' + htmlfigdir + figname + '" /> <br>'

    if ShowRtime:
        # Theta step Magnitude
        figname = 'theta_mag.png'
        plt.figure(figsize=widefigsize)
        plt.title("Magnitude of Theta Step")
        plt.plot(range(2, R_gradmag[:, 2:].shape[1] + 2), R_gradmag[:, 2:].T)
        plt.autoscale(enable=True, axis='x', tight=True)
        plt.savefig(reconfigdir + figname)
        plt.savefig(reconfigdir + figname[:-3] + 'pdf')
        # plt.savefig(reconfigdir + figname[:-3] + 'eps')
        plt.close('all')
        page += '<img ' + widefigHTML + ' src="' + htmlfigdir + figname + '" /> <br>'

    if ShowPP:
        page += '<h3>Piercing Point</h3>\n'

        newvec = [np.array(i.piercing_point)-np.array(j.piercing_point)
                  for i, j in zip(cfglist, cfglist_given)]
        if cfglist_true is not None:
            truevec = [np.array(i.piercing_point)-np.array(j.piercing_point)
                       for i, j in zip(cfglist_true, cfglist_given)]
        origvec = [np.array(i.piercing_point)-np.array(j.piercing_point)
                   for i, j in zip(cfglist_given, cfglist_given)]

        # U, V
        for j, ax in enumerate(['u', 'v']):
            figname = ax + '.png'
            plt.figure(figsize=widefigsize)
            plt.title(ax.upper() + " estimate")
            new, = plt.plot([i[j] for i in newvec])
            orig, = plt.plot([i[j] for i in origvec])
            if cfglist_true is not None:
                true, = plt.plot([i[j] for i in truevec])
                plt.legend([new, orig, true], ["Estimated Translation",
                                               "Initialization",
                                               "Ground Truth Translation"])
            else:
                plt.legend([new, orig], ["Estimated Translation",
                                         "Initialization"])
            plt.autoscale(enable=True, axis='x', tight=True)
            plt.savefig(reconfigdir + figname)
            plt.savefig(reconfigdir + figname[:-3] + 'pdf')
            # plt.savefig(reconfigdir + figname[:-3] + 'eps')
            plt.close('all')
            page += '<img ' + widefigHTML + ' src="' + htmlfigdir + figname + '" /> <br>'

    if ShowPPtime:
        # U over time
        figname = 'u_time.png'
        plt.figure(figsize=widefigsize)
        plt.title("U estimate over time")
        plt.plot(u_time.T)
        plt.autoscale(enable=True, axis='x', tight=True)
        plt.savefig(reconfigdir + figname)
        plt.savefig(reconfigdir + figname[:-3] + 'pdf')
        # plt.savefig(reconfigdir + figname[:-3] + 'eps')
        plt.close('all')
        page += '<img height="250" width="1041" src="' + htmlfigdir + figname + '" /> <br>'

        # V over time
        figname = 'v_time.png'
        plt.figure(figsize=widefigsize)
        plt.title("V estimate over time")
        plt.plot(v_time.T)
        plt.autoscale(enable=True, axis='x', tight=True)
        plt.savefig(reconfigdir + figname)
        plt.savefig(reconfigdir + figname[:-3] + 'pdf')
        # plt.savefig(reconfigdir + figname[:-3] + 'eps')
        plt.close('all')
        page += '<img height="250" width="1041" src="' + htmlfigdir + figname + '" /> <br>'

    if ShowSID:
        page += '<h3>SID Estimate</h3>\n'
        # newvec = [i.sid-j.sid for i, j in zip(cfglist, cfglist_given)]
        # truevec = [i.sid-j.sid for i, j in zip(cfglist_true, cfglist_given)]
        # origvec = [i.sid-j.sid for i, j in zip(cfglist_given, cfglist_given)]
        newvec = [i.sid for i in cfglist]
        if cfglist_true is not None:
            truevec = [i.sid for i in cfglist_true]
        origvec = [i.sid for i in cfglist_given]

        # F
        figname = 'sid.png'
        plt.figure(figsize=widefigsize)
        plt.title("SID estimate")
        new, = plt.plot(newvec)
        orig, = plt.plot(origvec)
        if cfglist_true is not None:
            true, = plt.plot(truevec)
            plt.legend([new, orig, true], ["Estimated Translation",
                                           "Initialization",
                                           "Ground Truth Translation"])
        else:
            plt.legend([new, orig], ["Estimated Translation",
                                     "Initialization"])
        plt.autoscale(enable=True, axis='x', tight=True)
        plt.savefig(reconfigdir + figname)
        plt.savefig(reconfigdir + figname[:-3] + 'pdf')
        # plt.savefig(reconfigdir + figname[:-3] + 'eps')
        plt.close('all')
        page += '<img ' + widefigHTML + ' src="' + htmlfigdir + figname + '" /> <br>'

    if ShowSIDtime:
        # sid over time
        figname = 'sid_time.png'
        plt.figure(figsize=widefigsize)
        plt.title("SID estimate over time")
        plt.plot(sid_time.T)
        plt.autoscale(enable=True, axis='x', tight=True)
        plt.savefig(reconfigdir + figname)
        plt.savefig(reconfigdir + figname[:-3] + 'pdf')
        # plt.savefig(reconfigdir + figname[:-3] + 'eps')
        plt.close('all')
        page += '<img height="250" width="1041" src="' + htmlfigdir + figname + '" /> <br>'

    if np.count_nonzero(~np.isnan(energy)) > 0:
        page += '<h2>Energy</h2>\n'

        figname = 'energy.png'
        plt.figure(figsize=widefigsize)
        plt.title("Energy")
        plt.plot(range(TestGradIter+1, energy[:, :].shape[1]),
                 energy[:, TestGradIter+1:].T)
        plt.autoscale(enable=True, axis='x', tight=True)
        plt.savefig(reconfigdir + figname)
        plt.savefig(reconfigdir + figname[:-3] + 'pdf')
        # plt.savefig(reconfigdir + figname[:-3] + 'eps')
        plt.close('all')
        page += '<img ' + widefigHTML + ' src="' + htmlfigdir + figname + '" /> <br>'

        figname = 'energyTotal.png'
        plt.figure(figsize=widefigsize2)
        plt.title("Geometry Functional")
        enx = range(TestGradIter+1, energy[:, :].shape[1])
        entot = energy[:, TestGradIter+1:]
        entotmean = -np.mean(entot, 0)
        plt.plot(enx, entotmean, 'b', linewidth=2)

        plt.autoscale(enable=True, axis='x', tight=True)
        plt.ylabel('NCC', fontsize=28)
        plt.xlabel('Iteration', fontsize=28)
        plt.autoscale(enable=True, axis='x', tight=True)
        plt.setp(plt.gca().get_xticklabels(), fontsize=20)
        plt.setp(plt.gca().get_yticklabels(), fontsize=20)
        plt.gcf().tight_layout()
        plt.savefig(reconfigdir + figname)
        plt.savefig(reconfigdir + figname[:-3] + 'pdf')
        # plt.savefig(reconfigdir + figname[:-3] + 'eps')
        plt.close('all')
        page += '<img ' + widefigHTML2 + ' src="' + htmlfigdir + figname + '" /> <br>'

    if len(LL[0]) > 0:
        page += '<h2>Log Posterior</h2>\n'

        figname = 'loglikelihood.png'
        fig, ax = plt.subplots(figsize=widefigsize)
        plt.title("Log Posterior")
        # plt.plot(LL[0], LL[1])
        for x0, x1, y0, y1 in zip(LL[0][:-1], LL[0][1:], LL[1][:-1], LL[1][1:]):
            if x1 - np.floor(x1) == .5:  # is geom est
                ax.plot([x0, x1], [y0, y1], color='r')
            else:
                ax.plot([x0, x1], [y0, y1], color='b')
        plt.autoscale(enable=True, axis='x', tight=True)
        plt.savefig(reconfigdir + figname)
        plt.savefig(reconfigdir + figname[:-3] + 'pdf')
        # plt.savefig(reconfigdir + figname[:-3] + 'eps')
        plt.close('all')
        page += '<img ' + widefigHTML + ' src="' + htmlfigdir + figname + '" /> <br>'

        # save figure of log likelihood only updates and
        figname = 'loglikelihoodonly.png'
        x = [xi for xi, yi in zip(LL[0], LL[1]) if xi-np.floor(xi) == .5]
        y = [yi for xi, yi in zip(LL[0], LL[1]) if xi-np.floor(xi) == .5]
        if len(x) > 0:
            plt.figure(figsize=widefigsize2)
            plt.title("Log Posterior", fontsize=32)
            plt.plot(x, y, 'b', linewidth=2)
            # new, = plt.plot([abs(nv[j] - newvecmean[j] - tv[j] + truevecmean[j])
            #                  for nv, tv in zip(newvec, truevec)], 'b', linewidth=2)
            # orig, = plt.plot([abs(ov[j] - origvecmean[j] - tv[j] + truevecmean[j])
            #                   for ov, tv in zip(origvec, truevec)], 'r', linewidth=2)
            # leg = plt.legend([orig, new], ["Nominal Error",
            #                                "New (estimated) Error"],
            #                  fontsize=26, fancybox=True)
            # leg.get_frame().set_alpha(0.5)
            plt.ylabel('Log Posterior', fontsize=28)
            plt.xlabel('Iteration', fontsize=28)
            plt.autoscale(enable=True, axis='x', tight=True)
            plt.setp(plt.gca().get_xticklabels(), fontsize=20)
            plt.setp(plt.gca().get_yticklabels(), fontsize=20)
            plt.gcf().tight_layout()
            plt.savefig(reconfigdir + figname)
            plt.savefig(reconfigdir + figname[:-3] + 'pdf')
            plt.savefig(reconfigdir + figname[:-3] + 'eps')
            plt.close('all')
            page += '<img ' + widefigHTML2 + ' src="' + htmlfigdir + figname + '" /> <br>'

    if len(Norm[0]) > 0:
        page += '<h2>Weighted Euclidean Norm</h2>\n'

        figname = 'euclidean.png'
        fig, ax = plt.subplots(figsize=widefigsize2)
        plt.title("Weighted Euclidean Norm")
        ax.plot(Norm[0], Norm[1])
        plt.ylabel('Norm', fontsize=28)
        plt.xlabel('Iteration', fontsize=28)
        plt.autoscale(enable=True, axis='x', tight=True)
        plt.setp(plt.gca().get_xticklabels(), fontsize=20)
        plt.setp(plt.gca().get_yticklabels(), fontsize=20)
        plt.gcf().tight_layout()
        plt.savefig(reconfigdir + figname)
        plt.savefig(reconfigdir + figname[:-3] + 'pdf')
        plt.close('all')
        page += '<img ' + widefigHTML2 + ' src="' + htmlfigdir + figname + '" /> <br>'

    if len(L2) > 0:
        page += '<h2>Ground Truth L2 Error</h2>\n'
        figname = 'L2Error.png'
        fig, ax = plt.subplots(figsize=widefigsize2)
        plt.title("Ground Truth L2 Error")
        ax.plot(L2)
        plt.ylabel('L2 Error', fontsize=28)
        plt.xlabel('Iteration', fontsize=28)
        plt.autoscale(enable=True, axis='x', tight=True)
        plt.setp(plt.gca().get_xticklabels(), fontsize=20)
        plt.setp(plt.gca().get_yticklabels(), fontsize=20)
        plt.gcf().tight_layout()
        plt.savefig(reconfigdir + figname)
        plt.savefig(reconfigdir + figname[:-3] + 'pdf')
        plt.close('all')
        page += '<img ' + widefigHTML2 + ' src="' + htmlfigdir + figname + '" /> <br>'


    page += HTMLGenFuncs.bodyHTMLEnd
    f = open(prefix + '.html', 'w')
    f.write(page)
    f.close()
    plt.close('all')
    print 'Wrote HTML Report ' + prefix


def SaveReport(reportobj):
    '''given the report object, save the report'''
    prefix = reportobj.cfscan.OutputFile
    # make the directories if they don't exist
    try:
        os.makedirs(prefix)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST:
            pass
        else: raise

    volest = reportobj.volest
    voltrue = reportobj.voltrue
    reportobj.volest = None     # clear so that we can write pkl
    reportobj.voltrue = None

    try:
        pickle.dump(reportobj, open(prefix + '/report.pkl', "wb"))
        reportobj.volest = volest
        reportobj.voltrue = voltrue
    except pickle.PicklingError:
        reportobj.volest = volest
        reportobj.voltrue = voltrue
        raise


def LoadReport(prefix, loadVol=True):
    '''loads a report pkl object'''
    reportobj = pickle.load(open(prefix + '/report.pkl', "rb"))
    # cfscan = Config.SpecToConfig(ReconConfig.ReconConfigSpecs)

    if loadVol:
        try:
            reportobj.volest = cc.LoadMHA(prefix + '/Imrecon.mha', ca.MEM_DEVICE)
        except IOError:
            print "LoadReport: Unable to load the volume"

    return reportobj
