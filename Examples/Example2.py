'''OSEM+TV reconstruction with noisy projections (no geometry estimation).
'''
import PyCACalebExtras.SetBackend
# plt = PyCACalebExtras.SetBackend.SetBackend('cairo')
plt = PyCACalebExtras.SetBackend.SetBackend('gtk')
import PyCA.Core as ca
import PyCACalebExtras.Common as cc
import PyCACalebExtras.Display as cd
from AppUtils import Config
from ConebeamRecon import ReconPoseEst, ReconConfig, CreateProjData, ReconReport
import copy
import ConebeamProjection as cp

##################################################
# Create a simple scan (with correct geometry) with some noise
##################################################

# Create simple volume for the projection dataset
vol = ca.Image3D(ca.GridInfo(ca.Vec3Di(50, 50, 50)), ca.MEM_DEVICE)
vol.setOrigin(ca.Vec3Df(-25, -25, -25))
cc.CreateSphere(vol, 19)
cc.AddSphere(vol, 17, val=-0.6)
cc.AddSphere(vol, 5, center=[-8, 5, 2], val=0.8)
cc.AddSphere(vol, 5, center=[8, 5, 2], val=0.8)

# create a scan config object
cfscan = Config.SpecToConfig(ReconConfig.ReconConfigSpecs)

# ***Simulation Scan Parameters***
cfscan.ScanParam.numProj = 200
cfscan.ScanParam.scanRange = 220
cfscan.ScanParam.SID = 500
cfscan.ScanParam.ImNoise = 4000

# simulate noisy projections for the dataset
cfglist_given, cfglist_true = CreateProjData.SetupScan(cfscan)
cfglist = copy.deepcopy(cfglist_given)  # estimates go in here
projgrid = cc.MakeGrid([100, 100, 1], [1.2, 1.2, 1], 'center')
projlist = CreateProjData.ProjectData(cfglist_true, cfscan, projgrid, vol=vol)
volgrid = cc.MakeGrid([128, 128, 128], [1, 1, 1], 'center')
volest = ca.Image3D(volgrid, ca.MEM_DEVICE)


##################################################
# Reconstruct with OSEM+TV
##################################################

cfscan.OutputFile = 'Results/Example2'

# ***Reconstruction Parameters***
cfscan.Recon.method = 'OSEM'
cfscan.Recon.numIters = 10
cfscan.Recon.TVweight = 0.02
cfscan.Recon.VolSize = [60, 60, 60]
cfscan.Recon.VolSpacing = [.8, .8, .8]
cfscan.Recon.VolOrigin = [-23.6, -23.6, -23.6]

# Reporting
report = ReconReport.report()
report.cfscan = cfscan
report.cfglist_given = cfglist_given
report.cfglist_true = cfglist_true

# Reconstruct
ReconPoseEst.RunRecon(cfglist, projlist, cfscan, report=report)
